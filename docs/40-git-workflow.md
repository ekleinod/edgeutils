# Git Workflow

!!! tip ""
	I use this workflow in one-person projects and small projects, I did not test it with medium sized or big projects.

See [Git Branching Model - Stable Mainline Model](../30-git-branching/) first.

## Basic rules

1. The `main` branch always contains a compileable, fully working version.

2. The `main` branch is never changed directly, i.e. is committed to.
	The only exception are documentation changes, they can be made directly in the main branch without ticket.

3. Feature and release branches are derived from and merged to the `main` branch.
	Feature and release branches are synchronized via the `main` branch, never directly.

4. Features are triggered by tickets and are implemented in feature branches.

5. Releases are prepared in special feature branches and are created in their own branches.

6. Every commit and tag is given a meaningful commit message.

## The feature workflow

1. A ticket with a number is created for a problem (in gitlab tickets are called issues):

	!!! quote ""
		Issue 23: Change color of exit button.

2. Assign a priority, a type and a status to the issue.
	While priority and type typically do not change during the process, you have to change the status accordingly throughout processing the ticket.
3. If the ticket it worked on, assign a person to it.
4. Create a feature branch, derive it from the `main` branch.

	!!! info ""
		Always check if you have the current state of the main branch.

	~~~ bash
	repopath $ git checkout main
	repopath $ git fetch -p
	repopath $ git pull
	repopath $ git checkout -b feature/t23-exit-button-color
	~~~

5. Do your work, this can be done in one or many commits, push the feature branch if neccessary.
	Always make sure you pushed your progress at the end of the day.

	!!! info ""
		Feature branches do not need to contain compileable, working code.

	~~~ bash
	repopath $ git add ...
	repopath $ git commit ...
	repopath $ git push
	~~~

6. If you think the feature is finished, check with the maintainer or any other quality checker if the feature is finished.
	Make sure, your code is compileable on it's own and after a merge with the `main` branch.
	If in doubt, merge the current `main` branch into the feature branch and test.
7. Update and commit all neccessary documentation, at least update the changelog.
8. Merge the feature branch into `main`, then delete it locally and remotely.

	!!! danger ""
		Always merge without fast forward "--no-ff" in order to maintain the commits of the feature branch.

	~~~ bash
	repopath $ git checkout main
	repopath $ git pull
	repopath $ git merge --no-ff feature/t23-exit-button-color
	repopath $ git push
	repopath $ git branch -d feature/t23-exit-button-color
	repopath $ git push --delete origin feature/t23-exit-button-color
	repopath $ git fetch -p
	~~~

9. Decide if a release has to be created or if further changes are collected before a release.


## The release workflow

1. Create a feature branch for the release, derive it from the `main` branch.
	This branch contains the changes for the release: documentation release info, version numbers etc.

	!!! info ""
		Always check if you have the current state of the main branch.

	~~~ bash
	repopath $ git checkout main
	repopath $ git fetch -p
	repopath $ git pull
	repopath $ git checkout -b feature/release-1.10.0
	~~~

2. Make the changes needed for the release.
	Do not forget the documentation, readme, and changelog.

	If you work overnight, push the branch, mostly the release branch is not pushed due to it's short living time.

3. If you think the release is ready, check with the maintainer or any other quality checker if the feature is finished.
	Make sure, your code is compileable, test.

4. Merge the feature branch into `main`, then delete it locally and, if neccessary, remotely.

	!!! danger ""
		Always merge without fast forward "--no-ff" in order to maintain the commits of the feature branch.

	~~~ bash
	repopath $ git checkout main
	repopath $ git pull
	repopath $ git merge --no-ff feature/release-1.10.0
	repopath $ git push
	repopath $ git branch -d feature/release-1.10.0
	repopath $ git fetch -p
	~~~

5. Create the release branch.

	If you create a major or minor release, create a new branch without the patch information:

	~~~ bash
	repopath $ git checkout main
	repopath $ git pull
	repopath $ git checkout -b release/1.10
	repopath $ git push
	~~~

	If you create a patch release, checkout the corresponding release branch and merge the `main` branch into it:

	~~~ bash
	repopath $ git checkout main
	repopath $ git pull
	repopath $ git checkout release/1.10
	repopath $ git pull
	repopath $ git merge --no-ff main
	repopath $ git push
	~~~

	Now, in both cases, the release branch contains the release.

6. Tag the release.

	The tag contains the patch information.

	~~~ bash
	repopath $ git tag -a 1.10.0
	repopath $ git push --tags
	~~~

	Go back to main in order to avoid problems of working in the release branches (trust me, btdt)

	~~~ bash
	repopath $ git checkout main
	repopath $ git fetch -p
	repopath $ git pull
	~~~

	!!! danger ""
		Make doubly sure that at this point all branches and tags are pushed to the repository.

7. If neccessary, create a release in gitlab, sourceforge, etc.

8. :fontawesome-regular-face-smile-wink:
