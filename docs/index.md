# Manual for Developers

![Logo](assets/icon.png){ align=right }

!!! tip ""
	*edgeutils* provides utilities for my projects.

## What it's about and what to expect

The edgeutils project is a collection of utilities for my projects.
This includes the following areas:

- Java development
	- Java classes
	- JAXB helper classes and schema/bindings files
- task automatization with ant tasks
- NSIS helper files

It is written for easing my projects and removing insufficiencies with existing implementations.

!!! warning
	Please feel free to use all edgeutils artifacts in your projects but beware of sudden changes until the release of a 1.0 version.

## License

edgeUtils is distributed under the terms of the GNU General Public License.

See [readme](https://gitlab.com/ekleinod/edgeutils/-/blob/main/README.md) or [COPYING](https://gitlab.com/ekleinod/edgeutils/-/blob/main/COPYING) for details.
