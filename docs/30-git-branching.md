# Git Branching Model - Stable Mainline Model

!!! tip ""
	I use this branching model in one-person projects and small projects, I did not test it with medium sized or big projects.


The stable mainline model is described in <http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/>.
I adapted the model for my needs, mainly introducing feature branches for preparation of releases.

## Main branch

"stable mainline" means there is always a stable mainline, the `main` branch.
This branch is always compileable and testable, both without errors.
It does not contain the latest release, but can contain development versions.

## Release branches

Releases are created in their own branches.
A new release branch is created for every major or minor release.
Patch releases are created in their respective minor release branches.
Every release is created or merged from the `main` branch.

Every release is tagged, tags contain the patch and, if needed, additional identifiers, such as rc1 or beta.

### Naming Release Branches

`release/major.minor`

- release/0.15
- release/1.0
- release/1.1

`release/1.0` would contain all `1.0` releases, such as `1.0.0-rc1`, `1.0.0`, `1.0.1`, and so forth.

### Naming Tags

`major.minor.patch[-additional]`

- 0.15.0
- 1.0.0-beta
- 1.0.0-rc1
- 1.0.0

!!! warning
	Do not confuse these releases with the [gitlab releases](https://gitlab.com/ekleinod/edgeutils/-/releases), here, "release" means a special branch and tag.


## Feature Branches

Features are developed using feature branches.
The names contain the ticket number if there is one and a short keyword describing the feature.

Special feature branches are used (different from the mainline model) for finalizing releases.
Feature branches are merged with no-fast-forward merges into `main`.


### Naming Feature Branches

`feature/t<ticket number>-<description>`
`feature/<description>`

- feature/t43-stable_mainline
- feature/t39-use_kotlin
- feature/documentation

### Naming Feature Branches for Finalizing Releases

`feature/release-major.minor.patch[-additional]`

- feature/release-0.15.0
- feature/release-1.0.0-rc1
- feature/release-1.0.0
