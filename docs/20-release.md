# How to create a release

!!! tip ""
	This one is more of a note for me :fontawesome-regular-face-smile-wink:

See [Git Workflow](../40-git-workflow/) for the overall workflow.
See [Sonatype OSSRH docs](https://central.sonatype.org/publish/publish-guide/) for upload guide.


All development branches (*feature*) are merged into the *main* branch.
The release is created from the *main* branch.
The examples will use release *0.17.0* as example.

If you are not sure about a step, save your progress in git, then execute the step.
This way you can check the status with `:::bash git status` and `:::bash git diff <file>` and go back to the last step, if needed, with `:::bash git checkout -- <file>`.

1. create a feature branch for finalizing the release (this differs from the mainline model, but I find it easier to work this way)

	~~~ bash
	repopath $ git checkout -b feature/release-0.17.0
	~~~

2. update copyright information

	1. update `build/project.properties`: properties and comment
	2. update `ant/ant-setcopyright`: comment
	3. update `README.md`: copyright section
	4. call *ant* in build directory

		~~~ bash
		repopath $ cd build
		repopath/build $ ant setcopyright
		Buildfile: repopath/build/build.xml

		clearLog:

		setcopyright:

		BUILD SUCCESSFUL
		Total time: 1 second
		~~~

3. check and update all release files if neccessary

	1. changelog.md
	2. README.md

4. update pom version and deploy to maven central


	1. update `edgeutils/pom.xml` with correct version
	2. same for `edgeutils/edgeutils/pom.xml`
	3. normal versions are uploaded to [maven central](https://search.maven.org/artifact/de.edgesoft/edgeutils), use "-SNAPSHOT" for snapshots, uploading to [nexus](https://oss.sonatype.org/#nexus-search;quick~edgeutils)
	4. run maven tests, there should be no errors or failures, run configuration `edgeutils clean compile test`
	5. make sure your credentials are in `~/.m2/settings.xml` which can be a symlink to a file in a backupped place
	6. make sure the right repo is in `edgeutils-modules/pom.xml` in `distributionManagement`
	7. make sure the gpg plugin is active (not commented out)
	8. run maven deploy, run configuration `edgeutils clean deploy`
	9. enter *gpg* password when prompted
	10. login to <https://oss.sonatype.org/>
	11. click on [Staging Repositories](https://oss.sonatype.org/#stagingRepositories), then select repository
	12. check in the lower panel if all artifacts are correct
	13. correct -> "close", incorrect -> "drop" with the buttons above the repo list
	14. confirm closing by entering a useful message, wait until the status changes (refresh for the impatient)
	15. check activity tab: if validation failed, drop the repo, fix the errors, then redeploy
	16. everything ok, status "closed" -> "release" with the button above the repo list, autodrop staging repo
	17. search in the [welcome tab](https://oss.sonatype.org/#welcome), [here](https://oss.sonatype.org/#nexus-search;quick~edgeutils) the new release should be found
	18. it will be found in [maven central](https://search.maven.org/artifact/de.edgesoft/edgeutils) after the next sync of the repo
	19. update dependencies in your other projects to the new edgeutils version

5. update documentation (especially the [Java part](../usage/java/))
6. check if all files look good
7. commit all changed files, check with status if you missed some files

	~~~ bash
	repopath $ git status
	repopath $ git add .
	repopath $ git commit
	~~~

8. merge and create release

	1. merge feature branch to master without fast forward

		~~~ bash
		repopath $ git checkout main
		repopath $ git merge --no-ff feature/release-0.17.0
		~~~

	2. remove local and remote feature branch

		~~~ bash
		repopath $ git branch -d feature/release-0.17.0
		repopath $ git push origin --delete feature/release-0.17.0
		~~~

	3. create release branch, tag release

		*If you created a patch release, checkout the corresponding release branch and merge `main` into it, then create a tag.*

		~~~ bash
		repopath $ git checkout -b release/0.15
		repopath $ git tag -a 0.17.0
		~~~

	4. back to `main`, push branches and tags

		~~~ bash
		repopath $ git checkout main
		repopath $ git push --all
		repopath $ git push --tags
		~~~

9. check if everything looks good, if there is an error, now is the time to panic :fontawesome-regular-grimace:

	- <https://gitlab.com/ekleinod/edgeutils>
	- <https://ekleinod.gitlab.io/edgeutils/>
	- <https://search.maven.org/artifact/de.edgesoft/edgeutils>

10. create a [gitlab release](https://gitlab.com/ekleinod/edgeutils/-/releases)
