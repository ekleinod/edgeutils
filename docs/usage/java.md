# Java

The Java part of *edgeutils* is provided via [Maven Central Repository](https://search.maven.org/search?q=edgeutils):

- <https://search.maven.org/artifact/de.edgesoft/edgeutils/>

Most common usage, see [maven repository page](https://search.maven.org/artifact/de.edgesoft/edgeutils/) for more examples (in these examples, version is `0.17.0`):

=== "Maven"

    ~~~ xml
	<dependency>
		<groupId>de.edgesoft</groupId>
		<artifactId>edgeutils</artifactId>
		<version>0.17.0</version>
	</dependency>
    ~~~

=== "Gradle Groovy"

    ~~~ groovy
	implementation 'de.edgesoft:edgeutils:0.17.0'
    ~~~

=== "Gradle Kotlin"

    ~~~ kotlin
	implementation("de.edgesoft:edgeutils:0.17.0")
    ~~~

You can use the Java classes directly per *eclipse* project import, according project files are in the repository.
As I am using *eclipse* for development, I do not provide project files for other IDEs, but there should be a mechanism for direct use of the Java files.
