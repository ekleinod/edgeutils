# Ant

The ant task files contain common tasks for different occasions.

## Usage

To use the tasks simply import them in your build file:

~~~ xml
<import file="ant-commons.xml" />
~~~

In git repositories I include *edgeutils* via submodules in order to have a stable import path to the ant tasks.


~~~ xml
<import file="../submodules/edgeutils/ant/ant-nsis.xml" />
~~~

## Ant files and their purpose

`ant-commons.xml`
: common tasks for all ant builds such as

	- help
	- clear
	- clearLog

`ant-dpkg.xml`
: creating *deb* installer via `dpkg`

`ant-jaxb.xml`
: calling `xjc` and deleting target directory

`ant-latex.xml`
: several LaTeX tasks

`ant-mallard.xml`
: several mallard tasks

`ant-mmd.xml`
: convert multimarkdown files to LaTeX files

`ant-nsis.xml`
: call NSIS on different operating systems

`ant-setversion.xml`
: setting version and copyright information in files
