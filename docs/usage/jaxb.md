# JAXB

There are several types of files provided for JAXB:

- commons schema
- bindings
- ant tasks (see Ant tasks)

## commons.xsd

`commons.xsd` contains some common xsd types.
To use the file, include it in your xsd file and use the structures as if they were written in your xsd file.

**Example: use Info type in yourdoc.xsd**

~~~ xml
<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:jxb="http://java.sun.com/xml/ns/jaxb"
            jxb:version="2.1">

  <!-- allow xml:base attribute in order to avoid include errors -->
  <xsd:import namespace="http://www.w3.org/XML/1998/namespace" schemaLocation="http://www.w3.org/2005/08/xml.xsd"/>

  <xsd:include schemaLocation="path to edgeutils/edgeutils/jaxb/commons.xsd"/>

  <xsd:element name="rootelement" type="RootElementType"/>

  <xsd:complexType name="RootElementType">
    <xsd:sequence>
      <xsd:element name="info" type="Info" minOccurs="1" maxOccurs="1" />
      <xsd:element name="content" type="xsd:string" minOccurs="1" maxOccurs="unbounded" />
    </xsd:sequence>
    <xsd:attribute ref="xml:base" />
  </xsd:complexType>

</xsd:schema>
~~~

The line importing `xml.xsd` is important as it provides the means to include a schema.
For a "real world" example see [:fontawesome-regular-file-code: refereemanager.xsd](https://gitlab.com/open-tt/refereemanager/blob/master/refereemanager/src/main/jaxb/refereemanager.xsd)

## commons.xjb etc.

`commons.xjb` and the other *xjb* files contains some default bindings.

`commons.xjb`
: bindings for the commons schema

`commons-only.xjb`
: bindings only for use in edgeutils context

`commons-reuse.xjb`
: bindings for all projects that use the commons schema

`bindings-empty.xjb`
: empty bindings file for proper use of the ant build task that needs three binding files in case you have only one or two

The default bindings change mainly the use of the new `LocalDateTime` classes and some strange choices of the JAXB developers such as the use of `BigInteger` instead of `Integer`.
The bindings files are easy to understand, check them for a complete list of changes.

To use the common bindings, declare them in the call of your JAXB compiler:

~~~ bash
$ xjc -npa -no-header -encoding UTF-8 \
  -d src \
  -p de.yourdoc \
  -b path/edgeutils/jaxb/commons.xjb \
  yourdoc.xsd
~~~

Useful options are:

`-npa`
: suppress generation of package level annotations (**/package-info.java) which are annoying for version control

`-no-header`
: suppress generation of a file header with timestamp, again annoying for version control

`-encoding`
: specify character encoding for generated source files

`-d`
: generated files will go into this directory

`-p`
: specifies the target package

`-b`
: specify external bindings files (each file must have its own -b)

It is possible to use several binding files, this used e.g. in the corresponding [ant task](../ant/):

~~~ bash
$ xjc -npa -no-header -encoding UTF-8 \
  -d src \
  -p de.yourdoc \
  -b path/edgeutils/jaxb/commons.xjb \
  -b path/edgeutils/jaxb/commons-reuse.xjb \
  -b yourbindings.xsd \
  yourdoc.xsd
~~~
