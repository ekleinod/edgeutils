# NSIS

The NSIS script provides a script for installing a jar file including entry in start menu and autostart folder.

To use the script set the needed variables and include the script.
For an example, please see [:fontawesome-regular-file-code: refereemanager.nsi](https://gitlab.com/open-tt/refereemanager/blob/master/build/win/refereemanager.nsi)

~~~ nsis
# use encoding: ISO-8859-15

!define PROJECT_NAME Refereemanager
!define VERSION 0.15.0
!define LONG_VERSION "0.15.0"
!define FILE_VERSION "0.15.0"
!define COMPANY "Ekkart Kleinod (edge-soft)"
!define URL http://www.edgesoft.de/
!define LONGNAME "refman - Der Referee-Manager"
!define FILENAME "refereemanager"

!define DIRNAME ${FILENAME}

!define DIR_RESOURCES "..\..\refereemanager\src\main\resources\"
!define DIR_FILES "..\..\files\"

!include ..\..\submodules\edgeutils\nsis\simple-jar.nsi
~~~
