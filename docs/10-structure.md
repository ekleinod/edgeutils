# Project structure

!!! tip ""
	Repositories, issues, files, ...

The git repository is maintained using gitlab.
gitlab is used for code maintenance and issue tracking.
Releases are tagged in the git repository, gitlab releases are created from these tags.

- <https://gitlab.com/ekleinod/edgeutils/>
- <https://gitlab.com/ekleinod/edgeutils/issues>
- <https://gitlab.com/ekleinod/edgeutils/tags>
- <https://gitlab.com/ekleinod/edgeutils/-/releases>

The files are structured as follows:

	edgeutils
	    ├ ant (common ant tasks)
	    │    ├ ant-commons.xml
	    │    ├ ant-dpkg.xml
	    │    ├ ant-jaxb.xml
	    │    ├ ant-latex.xml
	    │    ├ ant-mallard.xml
	    │    ├ ant-mmd.xml
	    │    ├ ant-nsis.xml
	    │    └ ant-setversion.xml
	    ├ build (ant task for building releases, mainly setting version information)
	    ├ docs (documentation)
	    ├ edgeutils (Java helper classes)
	    ├ jaxb (JAXB commons: mainly schemata, bindings)
	    ├ latex (LaTeX commmon files)
	    └ nsis (example NSIS project file)
