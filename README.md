# edgeUtils

The edgeutils project is a collection of utilities for my projects.
This includes the following areas:

- Java development
	- Java classes
	- JAXB helper classes and schema/bindings files
- task automatization with ant tasks
- NSIS helper files

It is written for easing my projects and removing insufficiencies with existing implementations.

There is a [documentation](https://ekleinod.gitlab.io/edgeutils/).

For an overview of all changes, see [changelog](changelog.md).

Feel free to use the utilities in your project.
Usage is described in the [documentation](https://ekleinod.gitlab.io/edgeutils/).


## Copyright

Copyright 2010-2022 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License.

This file is part of edgeUtils.

edgeUtils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

edgeUtils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with edgeUtils.  If not, see <http://www.gnu.org/licenses/>.
