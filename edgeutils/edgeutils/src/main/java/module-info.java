/**
 * Module info file.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * <p>edgeutils is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.12.0
 */
module de.edgesoft.edgeutils {

	// module exports
	exports de.edgesoft.edgeutils;
	exports de.edgesoft.edgeutils.collections;
	exports de.edgesoft.edgeutils.commandline;
	exports de.edgesoft.edgeutils.commons;
	exports de.edgesoft.edgeutils.commons.ext;
	exports de.edgesoft.edgeutils.datetime;
	exports de.edgesoft.edgeutils.files;
	exports de.edgesoft.edgeutils.freemarker;
	exports de.edgesoft.edgeutils.i18n;
	exports de.edgesoft.edgeutils.javafx;
	exports de.edgesoft.edgeutils.javafx.controller;
	exports de.edgesoft.edgeutils.jaxb;
	exports de.edgesoft.edgeutils.log4j;
	exports de.edgesoft.edgeutils.markup.latex;
	exports de.edgesoft.edgeutils.markup.multimarkdown;
	exports de.edgesoft.edgeutils.markup.reveal;
	exports de.edgesoft.edgeutils.testfx;
	exports de.edgesoft.edgeutils.xchart;

	// opens
	opens de.edgesoft.edgeutils.jaxb to java.xml.bind;
	// cannot give module to open to, because it is an unnamed module.
	opens de.edgesoft.edgeutils.commons;

	// simple requirements
	requires java.naming; // dependency of org.testfx, not loaded if org.testfx is required transitive
	requires java.xml.bind;
	requires jdk.xml.dom;
	requires org.apache.logging.log4j.core;
	requires org.testfx;

	// transitive requirements
	requires transitive java.prefs;
	requires transitive javafx.controls;
	requires transitive javafx.fxml;
	requires transitive javafx.graphics;
	requires transitive javafx.swing;
	requires transitive org.apache.logging.log4j;

	// transitive requirements with unstable names - no module info in the package
	requires transitive batik.dom;
	requires transitive batik.transcoder;
	requires transitive freemarker;
	requires transitive xchart;

	// requirements with unstable names - no module info in the package
	requires commons.cli;
	requires hamcrest.core;

}

/* EOF */
