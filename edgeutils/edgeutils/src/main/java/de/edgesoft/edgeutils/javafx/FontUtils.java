package de.edgesoft.edgeutils.javafx;

import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;


/**
 * Utilities for {@link Font}.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.11.0
 */
public class FontUtils {

	/**
	 * Returns derived font with given weight.
	 *
	 * @param theFont font
	 * @param theWeight font weight
	 *
	 * @return derived font
	 */
	public static Font getDerived(final Font theFont, final FontWeight theWeight) {
		return getDerived(theFont, theWeight, 0);
	}

	/**
	 * Returns derived font with given weight and relative size.
	 *
	 * @param theFont font
	 * @param theWeight font weight
	 * @param theSize relative font size (+/- int)
	 *
	 * @return derived font
	 */
	public static Font getDerived(final Font theFont, final FontWeight theWeight, final int theSize) {
		return getDerived(theFont, theWeight, null, theSize);
	}

	/**
	 * Returns derived font with given posture.
	 *
	 * @param theFont font
	 * @param thePosture font posture
	 *
	 * @return derived font
	 */
	public static Font getDerived(final Font theFont, final FontPosture thePosture) {
		return getDerived(theFont, thePosture, 0);
	}

	/**
	 * Returns derived font with given posture and relative size.
	 *
	 * @param theFont font
	 * @param thePosture font posture
	 * @param theSize relative font size (+/- int)
	 *
	 * @return derived font
	 */
	public static Font getDerived(final Font theFont, final FontPosture thePosture, final int theSize) {
		return getDerived(theFont, null, thePosture, theSize);
	}

	/**
	 * Returns derived font with given weight, posture and relative size.
	 *
	 * @param theFont font
	 * @param theWeight font weight
	 * @param thePosture font posture
	 * @param theSize relative font size (+/- int)
	 *
	 * @return derived font
	 */
	public static Font getDerived(final Font theFont, final FontWeight theWeight, final FontPosture thePosture, final int theSize) {
		return Font.font(theFont.getFamily(), theWeight, thePosture, theFont.getSize() + theSize);
	}

}

/* EOF */
