package de.edgesoft.edgeutils.javafx;

import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Labeled;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.image.ImageView;


/**
 * Utilities for {@link Button}.
 *
 * <p>When trying Kotlin, all static methods are candidates for
 * extending the button class.</p>
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.10.0
 */
public class ButtonUtils {

	/**
	 * Adapts button to match menu item.
	 *
	 * @param theButton button to adapt
	 * @param theMenuItem menu item to adapt from
	 */
	public static void adaptButton(
			ButtonBase theButton,
			final MenuItem theMenuItem
			) {

		if (theMenuItem.getGraphic() != null) {
			theButton.setGraphic(new ImageView(((ImageView) theMenuItem.getGraphic()).getImage()));
		}

		theButton.setTooltip(TooltipUtils.createTooltip(theMenuItem.getText(), theMenuItem.getAccelerator()));

	}

	/**
	 * Adapts button to match menu item including action.
	 *
	 * @param theButton button to adapt
	 * @param theMenuItem menu item to adapt from
	 */
	public static void adaptButtonWithAction(
			ButtonBase theButton,
			final MenuItem theMenuItem
			) {

		adaptButton(theButton, theMenuItem);
		theButton.setOnAction(theMenuItem.getOnAction());

	}

	/**
	 * Binds button disable property to selection of list view.
	 *
	 * @param theButton button to disable
	 * @param theListView list view with items
	 *
	 * @since 0.10.1
	 */
	public static void bindDisable(ButtonBase theButton, final ListView<?> theListView) {

		theButton.disableProperty().bind(
				theListView.getSelectionModel().selectedItemProperty().isNull()
		);

	}

	/**
	 * Binds button disable property to selection of combo box.
	 *
	 * @param theButton button to disable
	 * @param theComboBox combo box with items
	 *
	 * @since 0.10.1
	 */
	public static void bindDisable(ButtonBase theButton, final ComboBox<?> theComboBox) {

		theButton.disableProperty().bind(
				theComboBox.getSelectionModel().selectedItemProperty().isNull()
		);

	}

	/**
	 * Binds button disable property to emptyness of labeled item.
	 *
	 * @param theButton button to disable
	 * @param theLabeled labeled item
	 *
	 * @since 0.12.0
	 */
	public static void bindDisable(
			ButtonBase theButton,
			final Labeled theLabeled
			) {

		theButton.disableProperty().bind(
				theLabeled.textProperty().isEmpty()
		);

	}

	/**
	 * Fills a {@link SplitMenuButton} with the given {@link MenuItem}s, using the first for the split button.
	 *
	 * @param theSplitButton split button to fill
	 * @param theMenuItems menu items (first will be used for split button itself)
	 *
	 * @since 0.12.0
	 */
	public static void fillSplitMenuButton(
			SplitMenuButton theSplitButton,
			final MenuItem ... theMenuItems
			) {

		if (theMenuItems.length == 0) {
			return;
		}

		adaptButtonWithAction(theSplitButton, theMenuItems[0]);

		if (theMenuItems.length > 1) {
			for (MenuItem menuItem : theMenuItems) {
				theSplitButton.getItems().add(MenuItemUtils.copyMenuItem(menuItem));
			}
		}

	}
	
}

/* EOF */
