package de.edgesoft.edgeutils.i18n;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.logging.log4j.Logger;

import javafx.scene.Node;
import javafx.scene.control.MenuItem;

/**
 * i18n class with methods for everyday internationalization.
 *
 * Inspired by <a href="https://softwareengineering.stackexchange.com/questions/256806/best-approach-for-multilingual-java-enum">https://softwareengineering.stackexchange.com/questions/256806/best-approach-for-multilingual-java-enum</a>
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.13.0
 */
public abstract class I18N {
	
	/**
	 * Resource key part delimiter.
	 */
	public final static String DELIMITER = ".";
	
	/**
	 * Reference prefix.
	 */
	public final static String REFERENCE_PREFIX = "ref::";

	/**
	 * Resource bundle name pattern.
	 */
	public final static String BUNDLE_NAME = "%s.resources.i18n.%sTexts";

	
	/**
	 * Path to i18n texts.
	 */
	private static String sFilePath = null;

	/**
	 * Root path.
	 */
	private static String sRootPath = null;
	
	/**
	 * Optional logger.
	 */
	private static Optional<Logger> logger = null;

	/**
	 * Resource bundle for getting the texts (singleton).
	 */
	private static ResourceBundle resBundle = null;

	
	/**
	 * Initialize class with root class and default locale.
	 *
	 * @param theRootClass root class
	 */
	public static void init(
			final Class<?> theRootClass
			) {
		init(theRootClass, null, null);
	}

	/**
	 * Initialize class with root class and locale.
	 *
	 * @param theRootClass root class
	 * @param theLocale current locale (null for default)
	 */
	public static void init(
			final Class<?> theRootClass,
			final Locale theLocale
			) {
		init(theRootClass, theLocale, null);
	}

	/**
	 * Initialize class with root class, locale, and logger.
	 *
	 * @param theRootClass root class
	 * @param theLocale current locale (null for default)
	 * @param theLogger logger (null for none)
	 */
	public static void init(
			final Class<?> theRootClass,
			final Locale theLocale,
			final Logger theLogger
			) {

		assert (theRootClass != null) : "Root class must not be null.";

		sRootPath = theRootClass.getPackage().getName();
		sFilePath = String.format("%s.resources.i18n.%sTexts", sRootPath, theRootClass.getSimpleName());
		
		setLocale(theLocale);
		
		logger = Optional.ofNullable(theLogger);

	}

	/**
	 * Set locale.
	 *
	 * @param theLocale new locale (null for default)
	 */
	public static void setLocale(
			final Locale theLocale
			) {

		Locale.setDefault(Objects.requireNonNullElse(theLocale, Locale.getDefault()));

	}

	/**
	 * Returns the resource bundle.
	 *
	 * @return resource bundle
	 */
    private static ResourceBundle getResourceBundle() {

		assert (sFilePath != null) : "I18N file path must not be null";

        if (resBundle == null) {
            resBundle = ResourceBundle.getBundle(sFilePath, Locale.getDefault());
        }

        return resBundle;

    }
    
    /**
     * Determines whether the given <code>key</code> is contained in
     * this <code>ResourceBundle</code> or its parent bundles.
     *
     * @param theKey
     *        the resource <code>key</code>
     * @return <code>true</code> if the given <code>key</code> is
     *        contained in this <code>ResourceBundle</code> or its
     *        parent bundles; <code>false</code> otherwise.
     * @throws NullPointerException
     *         if <code>key</code> is <code>null</code>
     */
    public static boolean containsKey(String theKey) {
    	return getResourceBundle().containsKey(theKey);
    }
    
    
    /**
     * Creates resource key from parts, delimited by {@link #DELIMITER}.
     *  
     * @param theKeyParts parts of the key
     * @return resource key
     */
    public static String createKey(final String ... theKeyParts) {
    	return String.join(DELIMITER, theKeyParts).toLowerCase();
    }
    

    /**
	 * Returns a text for the given key formatted with the text arguments.
     *
     * @param theKey key of the text
     * @param theArguments arguments for the text
     * @return formatted text
     */
    public static Optional<String> getText(
    		final String theKey,
    		final Object ... theArguments
    		) {
    	
    	if (containsKey(theKey)) {
    		
    		String sReturn = getResourceBundle().getString(theKey);
    		
    		if (sReturn.startsWith(REFERENCE_PREFIX)) {
    			return getText(sReturn.substring(REFERENCE_PREFIX.length()), theArguments);
    		}
    		
    		return Optional.ofNullable(MessageFormat.format(sReturn, theArguments));
    		
    	}
    	
		logger.ifPresent(lgr -> lgr.debug(String.format("Missing resource for key '%s'", theKey)));
		return Optional.empty();
        
    }

    
    /**
     * Returns the key of the given enum value.
     *
     * @param theEnum enum value
     * @return key
     */
    public static String getKey(
    		Enum<?> theEnum
    		) {

    	return createKey(ResourcePrefix.ENUM.value(),
				theEnum.getClass().getPackageName(), 
				theEnum.getClass().getSimpleName(), 
				theEnum.name()
				).replace(String.format("%s.", sRootPath), "");

    }

    /**
     * Returns the text for the given enum value formatted with the text arguments.
     *
     * @param theEnum enum value
     * @param theArguments arguments for the text
     * @return text for the enum value
     */
    public static Optional<String> getText(
    		final Enum<?> theEnum,
    		final Object ... theArguments
    		) {
        return getText(getKey(theEnum), theArguments);
    }

    
    /**
     * Returns the key for the view indicated by the view controller.
     * 
     * Originally, the node id was used by calling {@link Node#getId()} here,
     * but the method should be usable for {@link MenuItem}s as well which are
     * no nodes for whatever reasons.
     * 
     * TODO: If this should change in the future, use a node instead.
     *
     * @param theController view controller
     * @param theNodeID node
	 * @param theResourceType resource types
     * @return key
     */
    public static String getViewNodeKey(
    		final Object theController,
    		final String theNodeID,
    		final ResourceType theResourceType
    		) {

    	return createKey(
				theController.getClass().getPackageName(),
				theController.getClass().getSimpleName(),
				theNodeID,
				theResourceType.value()
				).replace(".controller", ".view")
    			.replace("controller", "")
    			.replace(String.format("%s.", sRootPath), "");

    }

    /**
     * Returns the text for the view indicated by the view controller.
     *
     * @param theController view controller
     * @param theNodeID node id
	 * @param theResourceType resource types
     * @param theArguments arguments for the text
     * @return text
     */
    public static Optional<String> getViewNodeText(
    		final Object theController,
    		final String theNodeID,
    		final ResourceType theResourceType,
    		final Object ... theArguments
    		) {
        return getText(getViewNodeKey(theController, theNodeID, theResourceType), theArguments);
    }

}

/* EOF */
