package de.edgesoft.edgeutils.javafx;

import java.util.Arrays;
import java.util.List;

import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.edgeutils.i18n.ResourceType;
import javafx.scene.control.Tab;
import javafx.scene.image.ImageView;


/**
 * Utilities for {@link Tab}.
 *
 * <p>I wanted to reuse the code of {@link ControlUtils} for the fill methods of this class but
 * menu items and buttons do not share a common class, thus this is copy'n'paste
 * for the moment.</p>
 * 
 * <p>When trying Kotlin, all static methods are candidates for extending the menuitem class.</p>
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class TabUtils {

	/**
	 * Variable name prefix.
	 */
	public static final String PREFIX = "tab";

	
	/**
	 * Fills tab with resources.
	 * 
	 * @param theTab tab to fill
	 * @param theController view controller
	 * @param theResourceTypes resource types
	 */
	public static void fillTab(
			Tab theTab,
    		final Object theController,
    		final ResourceType ... theResourceTypes
			) {
		fillTab(theTab, theController, Resources.USE_DEFAULT_SIZE, theResourceTypes);
	}

	/**
	 * Fills tab with resources.
	 * 
	 * @param theTab tab to fill
	 * @param theController view controller
	 * @param theSize size of the icon
	 * @param theResourceTypes resource types
	 */
	public static void fillTab(
			Tab theTab,
    		final Object theController,
    		final int theSize,
    		final ResourceType ... theResourceTypes
			) {
		
		List<ResourceType> lstResourceTypes = (theResourceTypes.length == 0) ?
				Arrays.asList(ResourceType.values()) :
				Arrays.asList(theResourceTypes);
		
		String sID = ControlUtils.removePrefix(theTab.getId(), PREFIX);
		
		if (lstResourceTypes.contains(ResourceType.TEXT)) {
			I18N.getViewNodeText(theController, sID, ResourceType.TEXT).ifPresent(it -> theTab.setText(it));
		}
		
		if (lstResourceTypes.contains(ResourceType.ICON)) {
			I18N.getViewNodeText(theController, sID, ResourceType.ICON).ifPresent(it -> theTab.setGraphic(new ImageView(Resources.loadImage(it, theSize))));
		}
		
	}

	/**
	 * Fills tabs with resource.
	 * 
	 * @param theController view controller
	 * @param theResourceType resource types
	 * @param theTabs tab to fill
	 */
	public static void fillTabs(
    		final Object theController,
    		final ResourceType theResourceType,
    		Tab ... theTabs
			) {
		Arrays.asList(theTabs).stream().forEach(it -> fillTab(it, theController, theResourceType));
	}

}

/* EOF */
