package de.edgesoft.edgeutils.javafx;

import java.time.LocalDate;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import de.edgesoft.edgeutils.commons.ext.LocalDateAdapter;
import javafx.beans.property.SimpleObjectProperty;



/**
 * Adapter for {@link SimpleObjectProperty}.
 * 
 * <h3>Legal stuff</h3>
 * 
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 * 
 * <p>This file is part of edgeutils.</p>
 * 
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 * 
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 * 
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 * 
 * @author Ekkart Kleinod
 * @since 0.9.4
 */
public class SimpleObjectPropertyLocalDateAdapter extends XmlAdapter<String, SimpleObjectProperty<LocalDate>> {
	
	/**
	 * Marshal local date property.
	 * 
	 * @param theProperty local date property
	 * @return marshaled string
	 */
	@Override
	public String marshal(final SimpleObjectProperty<LocalDate> theProperty) throws Exception {
		return (theProperty == null) ? null : new LocalDateAdapter().marshal(theProperty.getValue());
	}
	
	/**
	 * Unmarshal local date property.
	 * 
	 * @param theString string
	 * @return local date property
	 */
	@Override
	public SimpleObjectProperty<LocalDate> unmarshal(final String theString) throws Exception {
		return (theString == null) ? null : new SimpleObjectProperty<>(new LocalDateAdapter().unmarshal(theString));
    }
	
}

/* EOF */
