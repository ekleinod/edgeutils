package de.edgesoft.edgeutils.javafx;

import de.edgesoft.edgeutils.files.Resources;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * Utility methods and constants for {@link Scene}.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.13.0
 */
public class SceneUtils {

	/**
	 * Returns new {@link Scene} with added local css stylesheets.
	 *
	 * TODO: read css files from file system/jar
	 *
	 * @param theParent parent (root) node
	 * @param theCSS css filenames with path
	 * @return new scene
	 */
	public static final Scene createScene(
			final Parent theParent,
			final String... theCSS
			) {

		Scene sceneReturn = new Scene(theParent);

		for (String sCSS : theCSS) {
			sceneReturn.getStylesheets().add(Resources.getExternalURI(sCSS));
		}

		return sceneReturn;

	}

}

/* EOF */
