package de.edgesoft.edgeutils.testfx;

import static org.testfx.matcher.base.GeneralMatchers.typeSafeMatcher;

import java.time.LocalDate;

import org.hamcrest.Factory;
import org.hamcrest.Matcher;

import javafx.scene.control.DatePicker;


/**
 * Matcher for {@link DatePicker}s.
 *
 * This class is needed until testfx provides the missing matcher.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.11.0
 */
public class DatePickerMatcher {

    /**
     * Creates a matcher that matches all {@link DatePicker}s that are empty.
     *
     * @return matcher for empty date pickers
     */
    @Factory
    public static Matcher<DatePicker> isEmpty() {
        return typeSafeMatcher(
        		DatePicker.class,
        		"is empty",
        		datePicker -> String.format("DatePicker's content: %s.", datePicker.getValue()),
        		datePicker -> (datePicker.getValue() == null)
        		);
    }

    /**
     * Creates a matcher that matches all {@link DatePicker}s that contain the given date.
     *
     * @param theDate date to check
     * @return matcher for date pickers with date
     */
    @Factory
    public static Matcher<DatePicker> hasDate(final LocalDate theDate) {
        return typeSafeMatcher(
        		DatePicker.class,
        		String.format("has date %s", theDate),
        		datePicker -> String.format("DatePicker's content: %s.", datePicker.getValue()),
        		datePicker -> ((datePicker.getValue() != null) && (datePicker.getValue().equals(theDate)))
        		);
    }

}

/* EOF */
