package de.edgesoft.edgeutils.javafx;

import javafx.scene.paint.Color;


/**
 * Class providing color operations.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.9.3
 */
public class ColorUtils {

	/**
	 * Formats color as web hex string (e.g. #FFFFFF) that can be read with {@link Color#web(String)}.
	 *
	 * @param theColor color
	 * @return web hex string
	 */
	public static String formatWebHex(final Color theColor) {
		return String.format( "#%02X%02X%02X",
	            (int)( theColor.getRed() * 255 ),
	            (int)( theColor.getGreen() * 255 ),
	            (int)( theColor.getBlue() * 255 ) );
	}
	
}

/* EOF */
