package de.edgesoft.edgeutils.testfx;

import static org.testfx.matcher.base.GeneralMatchers.typeSafeMatcher;

import org.hamcrest.Factory;
import org.hamcrest.Matcher;

import javafx.scene.control.CheckBox;


/**
 * Matcher for {@link CheckBox}es.
 *
 * <p>This class is needed until testfx contains an according matcher.</p>
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.11.0
 */
public class CheckBoxMatcher {

    /**
     * Creates a matcher that matches all {@link CheckBox}es that are selected (checked).
     *
     * @return matcher that matches selected checkboxes
     */
    @Factory
    public static Matcher<CheckBox> isSelected() {
        return typeSafeMatcher(
        		CheckBox.class,
        		"is selected",
        		checkBox -> String.format("CheckBox '%s' is selected: %b.", checkBox.getText(), checkBox.isSelected()),
        		checkBox -> checkBox.isSelected()
        		);
    }

    /**
     * Creates a matcher that matches all {@link CheckBox}es that are not selected (checked).
     *
     * @return matcher that matches not selected checkboxes
     */
    @Factory
    public static Matcher<CheckBox> isNotSelected() {
        return typeSafeMatcher(
        		CheckBox.class,
        		"is not selected",
        		checkBox -> String.format("CheckBox '%s' is selected: %b.", checkBox.getText(), checkBox.isSelected()),
        		checkBox -> !checkBox.isSelected()
        		);
    }

}

/* EOF */
