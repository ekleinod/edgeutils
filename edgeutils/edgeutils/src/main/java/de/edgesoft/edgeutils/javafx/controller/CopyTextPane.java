package de.edgesoft.edgeutils.javafx.controller;

import java.util.Objects;

import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.AnchorPane;

/**
 * Anchor pane with copy button and context menu.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.13.0
 */
public class CopyTextPane extends AnchorPane {
	
	/**
	 * Resource name of copy button.
	 */
	public static final String COPY_BUTTON = "icons/actions/edit-copy.svg";
	
	/**
	 * Factory method: create instance.
	 * 
	 * @param theContentLabel content label of pane
	 * @param theCopyButton resource name of copy button
	 * @param showCopyButton show copy button?
	 * 
	 * @return new instance
	 */
	public static CopyTextPane createInstance(
			final Labeled theContentLabel,
			final String theCopyButton,
			final boolean showCopyButton
			) {
		
		return new CopyTextPane(theContentLabel, theCopyButton, showCopyButton);
		
	}

	/**
	 * Factory method: create instance, no copy button.
	 * 
	 * @param theContentLabel content label of pane
	 * 
	 * @return new instance
	 */
	public static CopyTextPane createInstance(
			final Labeled theContentLabel
			) {
		
		return createInstance(theContentLabel, null, false);
		
	}

	/**
	 * Factory method: create instance, default copy button if chosen.
	 * 
	 * @param theContentLabel content label of pane
	 * @param showCopyButton show copy button?
	 * 
	 * @return new instance
	 */
	public static CopyTextPane createInstance(
			final Labeled theContentLabel,
			final boolean showCopyButton
			) {
		
		return createInstance(theContentLabel, null, showCopyButton);
		
	}

	/**
	 * Factory method: create instance, custom copy button.
	 * 
	 * @param theContentLabel content label of pane
	 * @param theCopyButton resource name of copy button
	 * 
	 * @return new instance
	 */
	public static CopyTextPane createInstance(
			final Labeled theContentLabel,
			final String theCopyButton
			) {
		
		return createInstance(theContentLabel, theCopyButton, true);
		
	}

	/**
	 * Factory method: create instance.
	 * 
	 * @param theContentText content text of pane
	 * @param theCopyButton resource name of copy button
	 * @param showCopyButton show copy button?
	 * 
	 * @return new instance
	 */
	public static CopyTextPane createInstance(
			final String theContentText,
			final String theCopyButton,
			final boolean showCopyButton
			) {
		
		return createInstance(new Label(theContentText), theCopyButton, showCopyButton);
		
	}

	/**
	 * Factory method: create instance, custom copy button.
	 * 
	 * @param theContentText content text of pane
	 * @param theCopyButton resource name of copy button
	 * 
	 * @return new instance
	 */
	public static CopyTextPane createInstance(
			final String theContentText,
			final String theCopyButton
			) {
		
		return createInstance(theContentText, theCopyButton, true);
		
	}

	/**
	 * Factory method: create instance, default copy button if chosen.
	 * 
	 * @param theContentText content text of pane
	 * @param showCopyButton show copy button?
	 * 
	 * @return new instance
	 */
	public static CopyTextPane createInstance(
			final String theContentText,
			final boolean showCopyButton
			) {
		
		return createInstance(theContentText, null, showCopyButton);
		
	}

	/**
	 * Factory method: create instance, no copy button.
	 * 
	 * @param theContentText content text of pane
	 * @return new instance
	 */
	public static CopyTextPane createInstance(
			final String theContentText
			) {
		
		return createInstance(theContentText, null, false);
		
	}

	/**
	 * Creates {@link CopyTextPane}.
	 *
	 * @param theContent content of pane
	 * @param theCopyButton resource name of copy button (null = default button resource)
	 * @param showCopyButton show copy button?
	 */
	private CopyTextPane(
			final Labeled theContent,
			final String theCopyButton,
			final boolean showCopyButton
			) {
		
		Objects.requireNonNull(theContent, "Missing labeled object for output.");
		String sCopyButton = Objects.requireNonNullElse(theCopyButton, COPY_BUTTON);

		getChildren().add(theContent);
		AnchorPane.setLeftAnchor(theContent, 0.0);
		AnchorPane.setRightAnchor(theContent, 50.0);
		
		if (showCopyButton) {

			Button btnCopyContent = new Button("", new ImageView(Resources.loadImage(sCopyButton, Prefs.SIZE_BUTTON_SMALL)));
			btnCopyContent.setOnAction(e -> copyText(theContent));
			btnCopyContent.setTooltip(new Tooltip("Text kopieren"));
	
			getChildren().add(btnCopyContent);
			AnchorPane.setRightAnchor(btnCopyContent, 0.0);
	
			ContextMenu mnuContext = new ContextMenu();
			MenuItem itmCopy = new MenuItem("Text kopieren", new ImageView(Resources.loadImage(sCopyButton, Prefs.SIZE_BUTTON_SMALL)));
			itmCopy.setOnAction(e -> copyText(theContent));
			mnuContext.getItems().addAll(itmCopy);
	
			theContent.setOnContextMenuRequested(e -> mnuContext.show((Node) e.getSource(), e.getScreenX(), e.getScreenY()));
			
		}

	}

	/**
	 * Copy text to clipboard.
	 *
	 * @param theTextHolder text holding label
	 */
	public static void copyText(
			final Labeled theTextHolder
			) {

		ClipboardContent clpContent = new ClipboardContent();
		clpContent.putString(theTextHolder.getText());
		Clipboard.getSystemClipboard().setContent(clpContent);

	}

}

/* EOF */
