package de.edgesoft.edgeutils.javafx;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;


/**
 * Utilities for {@link KeyCodeCombination}.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.11.0
 */
public class KeyCodeCombinationUtils {

	/**
	 * Returns list of {@link KeyCodeCombination}s for given string.
	 *
	 * @param theText text to convert to {@link KeyCodeCombination}s
	 *
	 * @return list of {@link KeyCodeCombination}s
	 */
	public static List<KeyCodeCombination> getKeyCodeCombinations(
			final String theText
			) {


		if (theText == null) {
			return Collections.emptyList();
		}

		List<KeyCodeCombination> lstReturn = new ArrayList<>();

		for (char theChar : theText.toCharArray()) {
			lstReturn.add(getKeyCodeCombination(theChar));
		}

		return lstReturn;

	}

	/**
	 * Returns {@link KeyCodeCombination} for given character.
	 *
	 * TODO correctly handle at-character
	 *
	 * @param theChar text to convert to {@link KeyCodeCombination}
	 * @return {@link KeyCodeCombination}
	 */
	public static KeyCodeCombination getKeyCodeCombination(
			final char theChar
			) {

		KeyCode theKeyCode = null;

		switch (theChar) {

			case '-':
				theKeyCode = KeyCode.MINUS;
				break;
			case '.':
				theKeyCode = KeyCode.PERIOD;
				break;
			case ' ':
				theKeyCode = KeyCode.SPACE;
				break;
			case '_':
				theKeyCode = KeyCode.UNDERSCORE;
				break;
			case '@':
				theKeyCode = KeyCode.AT;
				break;

			default:
				theKeyCode = KeyCode.getKeyCode(Character.toString(theChar).toUpperCase());
				break;
		}

		if (Character.isAlphabetic(theChar) && Character.isUpperCase(theChar)) {
			return new KeyCodeCombination(theKeyCode, KeyCombination.SHIFT_DOWN);
		}

		if (theKeyCode == KeyCode.AT) {
			return new KeyCodeCombination(KeyCode.A);
			// German keyboard: this does not work on Linux, I cannot press AltGr; not tested on non-german keyboards and windows
//			return new KeyCodeCombination(theKeyCode, KeyCombination.ALT_DOWN, KeyCombination.SHORTCUT_DOWN);
		}

		return new KeyCodeCombination(theKeyCode);

	}

}

/* EOF */
