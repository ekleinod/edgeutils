package de.edgesoft.edgeutils.javafx;

import java.time.LocalDate;

import org.apache.logging.log4j.util.Strings;

import de.edgesoft.edgeutils.datetime.DateTimeUtils;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Labeled;


/**
 * Utilities for {@link Labeled}.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.11.0
 */
public class LabeledUtils {

	/**
	 * Sets text from {@link StringProperty}.
	 *
	 * @param theLabel label to set text
	 * @param theText text to set
	 */
	public static void setText(
			Labeled theLabel, 
			final StringProperty theText
			) {

		theLabel.setText(
				(theText == null) ?
						null :
						theText.getValue());

	}

	/**
	 * Sets text from {@link ObjectProperty} with a date.
	 *
	 * @param theLabel label to set text
	 * @param theDate date to set
	 * @param thePattern date pattern
	 */
	public static void setText(
			Labeled theLabel, 
			final ObjectProperty<LocalDate> theDate, 
			final String thePattern
			) {

		theLabel.setText(
				(theDate == null) ?
						null :
						DateTimeUtils.formatDate(theDate.getValue(), thePattern));

	}

	/**
	 * Remove mnemonic marker from text.
	 *
	 * TODO removing mnemonic markers by {@link String#replace(CharSequence, CharSequence)} feels clumsy, maybe there is another way?
	 *
	 * @param theText text
	 * @return text without mnemonic marker
	 */
	public static String removeMnemonicMarker(final String theText) {
		
		return Strings.isBlank(theText) ? theText : theText.replace("_", "");

	}

}

/* EOF */
