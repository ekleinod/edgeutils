package de.edgesoft.edgeutils.log4j;

import java.io.StringWriter;
import java.io.Writer;

import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.WriterAppender;
import org.apache.logging.log4j.core.appender.WriterManager;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.layout.PatternLayout;

/**
 * Log4j {@link Appender} utilities.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.13.0
 */
public abstract class AppenderUtils {

	/**
	 * Adds and starts writer appender.
	 *
	 * @param theAppenderName name of appender
	 * @return writer
	 */
	public static Writer startWriterAppender(final String theAppenderName) {

		final Configuration config = LoggerContext.getContext(false).getConfiguration();
		final PatternLayout layout = PatternLayout.newBuilder()
				.withPattern("%d %-5p: %m%n")
				.withConfiguration(config)
				.build();

		final Writer wrtReturn = new StringWriter();
		final WriterAppender appender = WriterAppender.createAppender(layout, null, wrtReturn, theAppenderName, false, true);

		appender.start();
		config.addAppender(appender);
		config.getRootLogger().addAppender(appender, null, null);

		return wrtReturn;

	}

	/**
	 * Stops and removes appender.
	 *
	 * I hope, the {@link Writer} is closed when the appender is stopped but I am not sure.
	 * I would close the writer here but I cannot find a method to return the {@link Writer}
	 * from a {@link WriterAppender}or its {@link WriterManager}
	 *
	 * @param theAppenderName name of appender
	 */
	public static void stopAppender(final String theAppenderName) {

		final Configuration config = LoggerContext.getContext(false).getConfiguration();

		config.getAppender(theAppenderName).stop();

		config.getRootLogger().removeAppender(theAppenderName);
		config.getAppenders().remove(theAppenderName);

	}

}

/* EOF */
