
package de.edgesoft.edgeutils.commons;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import de.edgesoft.edgeutils.commons.ext.ModelClassExt;


/**
 * <p>Java class for AbstractModelClass complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractModelClass"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{}ModelClass"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractModelClass")
public abstract class AbstractModelClass
    extends ModelClassExt
{


}
