
package de.edgesoft.edgeutils.xchart;

/**
 * Color schemes based on color brewer schemes (http://colorbrewer2.org/).
 *
 * Colors from www.ColorBrewer.org by Cynthia A. Brewer, Geography, Pennsylvania State University.
 *
 * For enums I use the coding style of jaxb, so there will be no inconsistencies.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * <p>edgeutils is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.10.0
 */
public enum Colorschemes {

	Paired_qualitative_2,
	Paired_qualitative_3,

	PiYG_diverging_2,

	RdBu_diverging_9,

	RdYlGn_diverging_2,

	;

	private final String value;

	Colorschemes() {
			value = name().toLowerCase();
	}

	public String value() {
			return value;
	}

	public static Colorschemes fromValue(String v) {
		for (Colorschemes c: Colorschemes.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}

/* EOF */
