package de.edgesoft.edgeutils.javafx;

import java.awt.image.BufferedImage;

import org.apache.batik.transcoder.SVGAbstractTranscoder;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.ImageTranscoder;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;


/**
 * BufferedImageTranscoder for transcoding SVG to {@link BufferedImage} or {@link Image}.
 *
 * <p>Code inspired by <a href="https://gist.github.com/ComFreek/b0684ac324c815232556">https://gist.github.com/ComFreek/b0684ac324c815232556</a></p>
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.12.0
 */
public class SVGImageTranscoder extends ImageTranscoder {

	/**
	 * Singleton transcoder class for creation.
	 */
	private static SVGImageTranscoder svgTranscoder = null;

	/**
	 * Resulting buffered image.
	 */
	private BufferedImage bufImage = null;

	/**
	 * Sets image width.
	 *
	 * @param theWidth image width (-1 no width specified)
	 */
	public void setWidth(int theWidth) {
		getTranscodingHints().remove(SVGAbstractTranscoder.KEY_WIDTH);
		if (theWidth != -1) {
			addTranscodingHint(SVGAbstractTranscoder.KEY_WIDTH, Float.valueOf(theWidth));
		}
	}

	/**
	 * Sets image height.
	 *
	 * @param theHeight image height (-1 no height specified)
	 */
	public void setHeight(int theHeight) {
		getTranscodingHints().remove(SVGAbstractTranscoder.KEY_HEIGHT);
		if (theHeight != -1) {
			addTranscodingHint(SVGAbstractTranscoder.KEY_HEIGHT, Float.valueOf(theHeight));
		}
	}

	/**
	 * Creates new {@link SVGImageTranscoder} with given width and height.
	 *
	 * @param theWidth image width (-1 no width specified)
	 * @param theHeight image height (-1 no height specified)
	 * @return new transcoder
	 */
	public static SVGImageTranscoder createSVGImageTranscoder(
			final int theWidth,
			final int theHeight
			) {

		if (svgTranscoder == null) {
			svgTranscoder = new SVGImageTranscoder();
		}

		svgTranscoder.setWidth(theWidth);
		svgTranscoder.setHeight(theHeight);

		return svgTranscoder;

	}

	@Override
	public BufferedImage createImage(
			int theWidth,
			int theHeight
			) {

		return new BufferedImage(theWidth, theHeight, BufferedImage.TYPE_INT_ARGB);

	}

	@Override
	public void writeImage(
			BufferedImage img,
			TranscoderOutput to
			) throws TranscoderException {

		bufImage = img;

	}

	/**
	 * Returns buffered image.
	 *
	 * @return buffered image
	 */
	public BufferedImage getBufferedImage() {
		return bufImage;
	}

	/**
	 * Returns fx image.
	 *
	 * @return fx image
	 */
	public Image getFXImage() {
		return SwingFXUtils.toFXImage(getBufferedImage(), null);
	}

}

/* EOF */
