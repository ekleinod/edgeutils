package de.edgesoft.edgeutils.xchart;

import java.util.Date;
import java.util.List;

import org.knowm.xchart.XYSeries;
import org.knowm.xchart.internal.series.Series;

/**
 * Convenience methods for {@link XYSeries}.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * <p>edgeutils is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.10.1
 */
public class XYSeriesUtils {

	/**
	 * Returns initialized {@link XYSeries} object.
	 *
	 * @param name name
	 * @param xData x data list (null == empty)
	 * @param yData y data list (null == empty)
	 * @param errorBars error bars (null == empty)
	 * @param axisType axis type
	 *
	 * @return initialized xyseries object
	 */
	public static XYSeries getXYSeries(
			String name,
			List<? extends Date> xData,
			List<? extends Integer> yData,
			List<? extends Integer> errorBars,
			Series.DataType axisType
			) {

		return new XYSeries(name,
				(xData == null)
						? null
						: xData.stream().mapToDouble(d -> d.getTime()).toArray(),
				(yData == null)
						? null
						: yData.stream().mapToDouble(d -> d).toArray(),
				(errorBars == null)
						? null
						: errorBars.stream().mapToDouble(d -> d).toArray(),
				axisType);

	}

}

/* EOF */
