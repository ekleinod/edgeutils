package de.edgesoft.edgeutils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Throwable utilities, with kotlin extensions of the {@link Throwable} class.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class ThrowableUtils {

	/**
	 * Returns stacktrace as string.
	 *
	 * @param theThrowable throwable
	 * @return stacktrace as string
	 */
	public static String getStackTrace(
			Throwable theThrowable
			) {

		String sReturn = null;
		
		try (Writer wrtStackTrace = new StringWriter()) {
			
			theThrowable.printStackTrace(new PrintWriter(wrtStackTrace));
			sReturn = wrtStackTrace.toString();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return sReturn;

	}

}

/* EOF */
