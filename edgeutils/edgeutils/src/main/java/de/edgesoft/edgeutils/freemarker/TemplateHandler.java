package de.edgesoft.edgeutils.freemarker;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import de.edgesoft.edgeutils.files.Resources;
import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

/**
 * Freemarker template handler.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.12.0
 */
public class TemplateHandler {

	/**
	 * Path for templates.
	 */
	private static final String PATH_TEMPLATES = String.format("%s/%s", Resources.PATH_RESOURCES, "templates");


	/**
	 * Template configuration (singleton).
	 */
	private static Configuration tplConfig = null;

	/**
	 * Returns template configuration.
	 *
	 * @return configuration
	 */
	public static Configuration getConfiguration() {

		if (tplConfig == null) {

			tplConfig = new Configuration(Configuration.VERSION_2_3_28);
			tplConfig.setDefaultEncoding(StandardCharsets.UTF_8.name());
			tplConfig.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
			tplConfig.setLogTemplateExceptions(false);

		}

		return tplConfig;

	}

	/**
	 * Loads template.
	 *
	 * @param clsProvider provider class
	 * @param theTemplateName template filename
	 * @return template
	 * @throws IOException
	 */
	public static Template loadTemplate(
			final Class<? extends Object> clsProvider,
			final String theTemplateName
			) throws IOException {

		TemplateLoader tplLoader = new ClassTemplateLoader(clsProvider, PATH_TEMPLATES);
		getConfiguration().setTemplateLoader(tplLoader);

		return getConfiguration().getTemplate(theTemplateName);

	}

	/**
	 * Loads template with given name from given path.
	 *
	 * @param theTemplateName template filename
	 * @param theTemplatePath template path
	 * @return template
	 * @throws IOException
	 */
	public static Template loadTemplate(
			final String theTemplateName,
			final File theTemplatePath
			) throws IOException {

		getConfiguration().setDirectoryForTemplateLoading(theTemplatePath);
		return getConfiguration().getTemplate(theTemplateName);

	}

	/**
	 * Processes template, returns processed result.
	 *
	 * @param theTemplate template
	 * @param theDataModel data to be processed
	 *
	 * @return processed template content
	 * @throws IOException if string writer fails
	 * @throws TemplateException if template processing fails
	 * 
	 * @since 0.15.0
	 */
	public static String processTemplate(
			final Template theTemplate,
			final Object theDataModel
			) throws TemplateException, IOException {

		String sReturn = null;

		try (StringWriter wrtContent = new StringWriter()) {
			theTemplate.process(theDataModel, wrtContent);
			sReturn = wrtContent.toString();
		}

		return sReturn;

	}

	/**
	 * Processes template string, returns processed result.
	 * 
	 * There is no need to load a template, you can use a text string instead.
	 *
	 * @param theTemplateContent the template text
	 * @param theDataModel data to be processed
	 *
	 * @return processed template content
	 * @throws IOException if string writer fails
	 * @throws TemplateException if template processing fails
	 * 
	 * @since 0.15.0
	 */
	public static String processStringTemplate(
			final String theTemplateContent,
			final Object theDataModel
			) throws TemplateException, IOException {

		return processTemplate(new Template(null, new StringReader(theTemplateContent), TemplateHandler.getConfiguration()), theDataModel);

	}

}

/* EOF */
