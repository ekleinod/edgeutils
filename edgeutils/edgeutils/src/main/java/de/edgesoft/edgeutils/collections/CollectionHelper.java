package de.edgesoft.edgeutils.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Helpful methods for collections.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.2
 */
public class CollectionHelper {

	/**
	 * Returns a csv string of the given values.
	 *
	 * @param <T> stringable element type
	 * @param theCollection collection to stringify
	 * @param theSeparator separator string
	 * @return csv string (empty if collection is empty or any parameter is null)
	 */
	public static <T> String toCSVString(Collection<T> theCollection, String theSeparator) {

		if ((theCollection == null) || (theSeparator == null)) {
			return "";
		}

		StringBuffer sbReturn = new StringBuffer();

		boolean isFurther = false;
		for (T theElement : theCollection) {
			if (isFurther) {
				sbReturn.append(theSeparator);
			}
			if (theElement == null) {
				sbReturn.append(theElement);
			} else {
				sbReturn.append(theElement.toString().trim());
			}
			isFurther = true;
		}

		return sbReturn.toString();
	}

	/**
	 * Creates a list from the line with the given separator character.
	 *
	 * The method returns a list with as many entries as are expected,
	 * i.e. it fills the list with empty strings if needed.
	 *
	 * @param theLine line to split
	 * @param theSeparator separator between entries
	 * @return list with splitted, trimmed entries
	 */
	public static List<String> fromCSVString(String theLine, String theSeparator) {

		List<String> lstReturn = trimSplit(theLine, theSeparator);

		if (lstReturn == null) {
			return null;
		}

		// fill if separator is one character only
		if (theSeparator.length() == 1) {
			int iSepCount = countOccurrences(theLine, theSeparator.charAt(0));
			for (int i = lstReturn.size(); (i <= iSepCount); i++) {
				lstReturn.add("");
			}
		}

		return lstReturn;
	}

	/**
	 * Creates a list from the line with the given separator character, trimming all entries.
	 *
	 * @param theLine line to split
	 * @param theSeparator separator between entries
	 * @return list with splitted, trimmed entries
	 *
	 * @since 0.12.0
	 */
	public static List<String> trimSplit(String theLine, String theSeparator) {

		if ((theLine == null) || (theSeparator == null)) {
			return null;
		}

		List<String> lstReturn = new ArrayList<>();
		String sSeparator = (theSeparator.equals(".")) ? "\\." : theSeparator;

		if (theLine.trim().isEmpty()) {
			return lstReturn;
		}

		for (String theEntry : Arrays.asList(theLine.trim().split(sSeparator))) {
			lstReturn.add(theEntry.trim());
		}

		return lstReturn;
	}

	/**
	 * Returns the number of occurrences of needle in haystack.
	 *
	 * @param haystack string to search through
	 * @param needle character to find
	 * @return number of occurences of needle in haystack
	 */
	public static int countOccurrences(String haystack, char needle) {

		if (haystack == null) {
			return -1;
		}

		int iCount = 0;
		for (char c : haystack.toCharArray()) {
			if (c == needle) {
				iCount++;
			}
		}
		return iCount;
	}

	/**
	 * Returns if the collection contains only empty objects (i.e. every contained object.toString returns "").
	 *
	 * @param <T> element type
	 * @param theCollection collection to check
	 * @return does collection contain only empty objects (true == only empty objects; false == at least one nonempty object)
	 */
	public static <T> boolean isEmptyString(Collection<T> theCollection) {
		if (theCollection == null) {
			return false;
		}

		boolean bReturn = true;

		for (T theObject : theCollection) {
			if (!theObject.toString().isEmpty()) {
				bReturn = false;
			}
		}

		return bReturn;
	}

	/**
	 * Returns a list of the given size containing value.
	 *
	 * @param <T> element type
	 * @param theValue value to be filled with
	 * @param theSize size of the collection
	 * @return list containing value
	 */
	public static <T> List<T> getFilledList(T theValue, int theSize) {

		if (theValue == null) {
			return null;
		}

		if (theSize < 0) {
			return Collections.emptyList();
		}

		List<T> lstReturn = new ArrayList<>();
		for (int i = 0; i < theSize; i++) {
			lstReturn.add(theValue);
		}

		return lstReturn;
	}

}

/* EOF */
