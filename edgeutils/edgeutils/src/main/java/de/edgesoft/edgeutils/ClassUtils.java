package de.edgesoft.edgeutils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Utilities for {@link Class}.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * <p>edgeutils is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.10.0
 */
public class ClassUtils {

	/**
	 * Returns declared non private fields including class inheritance up to the last abstract class.
	 *
	 * @param theClass the class to get the fields for
	 * @return list of declared fields (non-private)
	 */
	public static List<Field> getDeclaredFieldsFirstAbstraction(final Class<?> theClass) {

		Objects.requireNonNull(theClass);

		List<Field> lstAllFields = new ArrayList<>();

        Class<?> clsTemp = theClass;
    	lstAllFields.addAll(Arrays.asList(clsTemp.getDeclaredFields()));

        while (Modifier.isAbstract(clsTemp.getSuperclass().getModifiers())) {
        	clsTemp = clsTemp.getSuperclass();
        	lstAllFields.addAll(Arrays.asList(clsTemp.getDeclaredFields()));
        }

		List<Field> lstReturn = new ArrayList<>();

		for (Field theField : lstAllFields) {
			if (!Modifier.isPrivate(theField.getModifiers())) {
				lstReturn.add(theField);
			}
		}

        return lstReturn;

	}


	/**
	 * Returns declared fields including class inheritance.
	 *
	 * @param theClass the class to get the fields for
	 * @return list of declared fields
	 */
	public static List<Field> getDeclaredFieldsInheritance(final Class<?> theClass) {

		Objects.requireNonNull(theClass);

		List<Field> lstReturn = new ArrayList<>();

        Class<?> clsTemp = theClass;
    	lstReturn.addAll(Arrays.asList(clsTemp.getDeclaredFields()));

        while (clsTemp.getSuperclass() != null) {
        	clsTemp = clsTemp.getSuperclass();
        	lstReturn.addAll(Arrays.asList(clsTemp.getDeclaredFields()));
        }

        return lstReturn;

	}

}

/* EOF */
