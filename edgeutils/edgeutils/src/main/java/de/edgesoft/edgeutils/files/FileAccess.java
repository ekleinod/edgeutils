package de.edgesoft.edgeutils.files;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class providing usual file access operations.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.1
 */
public class FileAccess {

	/** Standard encoding to use: UTF-8. */
	private static Charset theEncoding = StandardCharsets.UTF_8;

	/**
	 * Sets the file encoding.
	 *
	 * <p>For the standard encodings see java.nio.charsets.StandardCharsets.</p>
	 *
	 * <p>Use them as follows, example: UTF-8:</p>
	 *
	 * <pre>{@code
	 *   FileAccess.setEncoding(StandardCharsets.UTF_8);
	 * }</pre>
	 *
	 * @param newEncoding the file encoding
	 */
	public static void setEncoding(final Charset newEncoding) {
		Objects.requireNonNull(newEncoding, "encoding must not be null");
		theEncoding = newEncoding;
	}

	/**
	 * Returns the current file encoding.
	 *
	 * @return the file encoding
	 *
	 * @since 0.11.0
	 */
	public static Charset getEncoding() {
		return theEncoding;
	}

	/**
	 * Read content of a file.
	 *
	 * @param theFilePath filepath
	 * @return file content
	 *
	 * @throws Exception if one occurs
	 */
	public static String readFile(
			final Path theFilePath
			) throws Exception {

		Objects.requireNonNull(theFilePath, "filename must not be null");

		String sReturn = null;
		
		try (Stream<String> stmLines = Files.lines(theFilePath, getEncoding())) {
			sReturn = stmLines.collect(Collectors.joining(System.lineSeparator()));
		}

		return sReturn;
		
	}

	/**
	 * Write content to a file.
	 *
	 * This method creates the whole path to the file, if it does not exist.
	 *
	 * @param theFileName filename
	 * @param theContent file content
	 *
	 * @throws IOException if one occurs
	 */
	public static void writeFile(
			final Path theFileName, 
			final String theContent
			) throws IOException {

		Objects.requireNonNull(theFileName, "filename must not be null");
		Objects.requireNonNull(theContent, "content must not be null");

		writeFile(theFileName, Arrays.asList(theContent));
	}

	/**
	 * Write list content to a file.
	 *
	 * <p>This method creates the whole path to the file, if it does not exist.</p>
	 *
	 * @param theFileName filename
	 * @param theContent file content (list)
	 *
	 * @throws IOException if one occurs
	 *
	 * @since 0.5.0
	 */
	public static void writeFile(
			final Path theFileName, 
			final List<String> theContent
			) throws IOException {

		Objects.requireNonNull(theFileName, "filename must not be null");
		Objects.requireNonNull(theContent, "content must not be null");

		if (theFileName.getParent() != null) {
			Files.createDirectories(theFileName.getParent());
		}

		Files.write(theFileName, theContent, getEncoding());

	}

}

/* EOF */
