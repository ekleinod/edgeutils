package de.edgesoft.edgeutils.files;

import java.nio.file.Paths;

import javafx.beans.property.StringProperty;

/**
 * Class file utilities.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.9.7
 */
public class FileUtils {

	/**
	 * Returns a clean, special character free filename.
	 *
	 * @param theFilename the file name
	 * @return cleaned filename
	 */
	public static String cleanFilename(final String theFilename) {
		return cleanFilename(theFilename, true);
	}

	/**
	 * Returns a clean filename, i.e. free of special characters.
	 *
	 * @param theFilename the file name
	 * @param convertUmlauts convert umlauts too?
	 * @return cleaned filename
	 *
	 * @since 0.11.0
	 */
	public static String cleanFilename(final String theFilename, final boolean convertUmlauts) {

		String sConverted = theFilename;

		sConverted = sConverted
				.replace(" ", "_")
				.replace("?", "_")
				.replace(":", "_")
				.replace(";", "_")
				.replace(",", "_")
				.replace("/", "_")
				.replace("\\", "_")
				.replace("*", "_")
				.replace("(", "_")
				.replace(")", "_")
				.replace("[", "_")
				.replace("]", "_")
				.replace("{", "_")
				.replace("}", "_")
				;

		sConverted = sConverted
				.replace("–", "-")
				;
		
		if (convertUmlauts) {
			sConverted = sConverted
					.replace("ä", "ae")
					.replace("Ä", "Ae")
					.replace("ö", "oe")
					.replace("Ö", "Oe")
					.replace("ü", "ue")
					.replace("Ü", "Ue")
					.replace("ß", "ss")
					;
		}

		return sConverted;

	}

	/**
	 * Returns if file given by path and filename exists.
	 *
	 * @param thePath path
	 * @param theFilename filename
	 * @return does file exist?
	 *
	 * @since 0.11.0
	 */
	public static boolean existsFile(final String thePath, final StringProperty theFilename) {

		if (theFilename == null) {
			return false;
		}

		return existsFile(thePath, theFilename.getValueSafe());

	}

	/**
	 * Returns if file given by path and filename exists.
	 *
	 * @param thePath path
	 * @param theFilename filename
	 * @return does file exist?
	 *
	 * @since 0.11.0
	 */
	public static boolean existsFile(final String thePath, final String theFilename) {

		if ((theFilename == null) || theFilename.isEmpty()) {
			return false;
		}

		return Paths.get((thePath == null) ? "" : thePath, theFilename).toFile().exists();

	}

}

/* EOF */
