package de.edgesoft.edgeutils;

/**
 * Special exception (good coding style, I presume).
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.1
 */
public class EdgeUtilsException extends Exception {

	/** Default serial id. */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor with message.
	 *
	 * @param theErrorMessage error message
	 */
	public EdgeUtilsException(String theErrorMessage) {
		super(theErrorMessage);
	}

	/**
	 * Constructor with exception.
	 *
	 * @param theException exception
	 */
	public EdgeUtilsException(Exception theException) {
		super(theException);
	}

}

/* EOF */
