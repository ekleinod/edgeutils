package de.edgesoft.edgeutils.testfx;

import static org.testfx.matcher.base.GeneralMatchers.typeSafeMatcher;

import org.hamcrest.Factory;
import org.hamcrest.Matcher;

import javafx.scene.control.ComboBox;


/**
 * Matcher for {@link ComboBox}es.
 *
 * This class is needed until testfx provides the missing methods of this matcher.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.11.0
 */
public class ComboBoxMatcher {

    /**
     * Creates a matcher that matches all {@link ComboBox}es that are not selected.
     *
     * @return matcher for combo boxes that are not selected
     */
    @Factory
    public static Matcher<ComboBox<?>> isNotSelected() {
        return typeSafeMatcher(
        		ComboBox.class,
        		"is not selected",
        		comboBox -> String.format("ComboBox's selection: %s.", comboBox.getSelectionModel().getSelectedItem()),
        		comboBox -> (comboBox.getSelectionModel().getSelectedItem() == null)
        		);
    }

}

/* EOF */
