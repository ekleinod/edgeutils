package de.edgesoft.edgeutils.datetime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.util.StringConverter;



/**
 * Utility methods for date, time, and datetime.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.9.0
 */
public class DateTimeUtils {

	/**
	 * Standard pattern for dates.
	 *
	 * @since 0.9.2
	 */
	public static final String DATE_PATTERN = "dd.MM.yyyy";

	/**
	 * Standard pattern for times.
	 *
	 * @since 0.9.7
	 */
	public static final String TIME_PATTERN = "HH:mm";

	/**
	 * Standard pattern for datetimes.
	 *
	 * @since 0.9.7
	 */
	public static final String DATETIME_PATTERN = String.format("%s, %s", DATE_PATTERN, TIME_PATTERN);

	/**
	 * Standard patterns and formatters.
	 *
	 * <p>TODO: with Java 9 use Map.of(LocalDate.class, DateTimeFormatter.ofPattern("dd.MM.yyyy"), ...)</p>
	 *
	 * @since 0.12.0
	 */
	private static final Map<Class<? extends TemporalAccessor>, DateTimeFormatter> STANDARD_FORMATTERS = Collections.unmodifiableMap(new HashMap<Class<? extends TemporalAccessor>, DateTimeFormatter>() {
		private static final long serialVersionUID = 1L;
		{
			put(LocalDate.class, DateTimeFormatter.ofPattern(DATE_PATTERN));
			put(LocalTime.class, DateTimeFormatter.ofPattern(TIME_PATTERN));
			put(LocalDateTime.class, DateTimeFormatter.ofPattern(DATETIME_PATTERN));
		}
	});

	/**
	 * Returns formatter to given pattern.
	 *
	 * @param thePattern format pattern
	 * @return formatter, null if pattern is null
	 *
	 *  @since 0.12.0
	 */
	private static DateTimeFormatter getFormatter(
			final String thePattern
			) {
		return (thePattern == null) ? null : DateTimeFormatter.ofPattern(thePattern);
	}


	/**
	 * Format {@link TemporalAccessor} with standard pattern/formatter.
	 *
	 * @param theAccessor accessor
	 * @return formatted accessor
	 *
	 * @since 0.12.0
	 */
	public static String formatTemporalAccessor(
			final TemporalAccessor theAccessor
			) {
		return formatTemporalAccessor(theAccessor, getFormatter(null));
	}

	/**
	 * Format {@link TemporalAccessor} with given pattern.
	 *
	 * @param theAccessor accessor
	 * @param thePattern pattern (null = standard pattern)
	 * @return formatted accessor
	 *
	 * @since 0.9.7
	 */
	public static String formatTemporalAccessor(
			final TemporalAccessor theAccessor,
			final String thePattern
			) {
		return formatTemporalAccessor(theAccessor, getFormatter(thePattern));
	}

	/**
	 * Format {@link TemporalAccessor} with given {@link DateTimeFormatter}.
	 *
	 * @param theAccessor accessor
	 * @param theFormatter formatter (null = standard formatter)
	 * @return formatted accessor
	 *
	 * @since 0.12.0
	 */
	public static String formatTemporalAccessor(
			final TemporalAccessor theAccessor,
			final DateTimeFormatter theFormatter
			) {

		if (theAccessor == null) {
			return null;
		}

		DateTimeFormatter fmtFormatter = theFormatter;
		if (fmtFormatter == null) {
			fmtFormatter = STANDARD_FORMATTERS.get(theAccessor.getClass());
		}

		if (fmtFormatter == null) {
			return null;
		}

		return fmtFormatter.format(theAccessor);

	}


	/**
	 * Format {@link LocalDate} with standard pattern/formatter.
	 *
	 * @param theDate date
	 * @return formatted date
	 *
	 * @since 0.9.2
	 */
	public static String formatDate(
			final LocalDate theDate
			) {
		return formatDate(theDate, getFormatter(null));
	}

	/**
	 * Format {@link LocalDate} with given pattern.
	 *
	 * @param theDate date
	 * @param thePattern pattern (null = standard pattern)
	 * @return formatted date
	 *
	 * @since 0.9.7
	 */
	public static String formatDate(
			final LocalDate theDate,
			final String thePattern
			) {
		return formatDate(theDate, getFormatter(thePattern));
	}

	/**
	 * Format {@link LocalDate} with given formatter.
	 *
	 * @param theDate date
	 * @param theFormatter formatter (null = standard formatter)
	 * @return formatted date
	 *
	 * @since 0.12.0
	 */
	public static String formatDate(
			final LocalDate theDate,
			final DateTimeFormatter theFormatter
			) {
		return formatTemporalAccessor(theDate, theFormatter);
	}

	/**
	 * Format {@link LocalDateTime} with standard pattern/formatter.
	 *
	 * @param theDateTime datetime
	 * @return formatted datetime
	 *
	 * @since 0.9.7
	 */
	public static String formatDateTime(
			final LocalDateTime theDateTime
			) {
		return formatDateTime(theDateTime, getFormatter(null));
	}

	/**
	 * Format {@link LocalDateTime} with given pattern.
	 *
	 * @param theDateTime datetime
	 * @param thePattern pattern (null = standard pattern)
	 * @return formatted datetime
	 *
	 * @since 0.9.7
	 */
	public static String formatDateTime(
			final LocalDateTime theDateTime,
			final String thePattern
			) {
		return formatDateTime(theDateTime, getFormatter(thePattern));
	}

	/**
	 * Format {@link LocalDateTime} with given formatter.
	 *
	 * @param theDateTime datetime
	 * @param theFormatter formatter (null = standard formatter)
	 * @return formatted datetime
	 *
	 * @since 0.12.0
	 */
	public static String formatDateTime(
			final LocalDateTime theDateTime,
			final DateTimeFormatter theFormatter
			) {
		return formatTemporalAccessor(theDateTime, theFormatter);
	}

	/**
	 * Format {@link LocalTime} with standard pattern/formatter.
	 *
	 * @param theTime time
	 * @return formatted time
	 *
	 * @since 0.9.7
	 */
	public static String formatTime(
			final LocalTime theTime
			) {
		return formatTime(theTime, getFormatter(null));
	}

	/**
	 * Format {@link LocalTime} with given pattern.
	 *
	 * @param theTime time
	 * @param thePattern pattern (null = standard pattern)
	 * @return formatted time
	 *
	 * @since 0.9.7
	 */
	public static String formatTime(
			final LocalTime theTime,
			final String thePattern
			) {
		return formatTime(theTime, getFormatter(thePattern));
	}

	/**
	 * Format {@link LocalTime} with given formatter.
	 *
	 * @param theTime time
	 * @param theFormatter formatter (null = standard formatter)
	 * @return formatted time
	 *
	 * @since 0.12.0
	 */
	public static String formatTime(
			final LocalTime theTime,
			final DateTimeFormatter theFormatter
			) {
		return formatTemporalAccessor(theTime, theFormatter);
	}


	/**
	 * Format datetime as date.
	 *
	 * @param theDateTime datetime
	 * @return formatted datetime as date
	 *
	 * @since 0.9.7
	 */
	public static String formatDateTimeAsDate(
			final LocalDateTime theDateTime
			) {
		return (theDateTime == null) ? null : formatDate(theDateTime.toLocalDate());
	}

	/**
	 * Format datetime as time.
	 *
	 * @param theDateTime datetime
	 * @return formatted datetime as time
	 *
	 * @since 0.9.7
	 */
	public static String formatDateTimeAsTime(
			final LocalDateTime theDateTime
			) {
		return (theDateTime == null) ? null : formatTime(theDateTime.toLocalTime());
	}


	/**
	 * Parse date with standard pattern/formatter.
	 *
	 * @param theString date string
	 * @return date
	 *
	 * @since 0.9.2
	 */
	public static LocalDate parseDate(
			final String theString
			) {
		return parseDate(theString, getFormatter(null));
    }

	/**
	 * Parse date with given pattern.
	 *
	 * @param theString date string
	 * @param thePattern pattern (null = standard pattern)
	 * @return date
	 *
	 * @since 0.9.7
	 */
	public static LocalDate parseDate(
			final String theString,
			final String thePattern
			) {
		return parseDate(theString, getFormatter(thePattern));
    }

	/**
	 * Parse date with given formatter.
	 *
	 * @param theString date string
	 * @param theFormatter formatter (null = standard formatter)
	 * @return date
	 *
	 * @since 0.12.0
	 */
	public static LocalDate parseDate(
			final String theString,
			final DateTimeFormatter theFormatter
			) {

		if (theString == null) {
			return null;
		}

        try {
            return LocalDate.parse(theString, (theFormatter == null) ? STANDARD_FORMATTERS.get(LocalDate.class) : theFormatter);
        } catch (DateTimeParseException e) {
            return null;
        }
    }

	/**
	 * Is string valid date?
	 *
	 * @param theString date string
	 * @return valid date?
	 *
	 * @since 0.9.2
	 */
	public static boolean isValidDate(
			final String theString
			) {
        return isValidDate(theString, getFormatter(null));
    }

	/**
	 * Is string valid date?
	 *
	 * @param theString date string
	 * @param thePattern pattern (null = standard pattern)
	 * @return valid date?
	 *
	 * @since 0.9.7
	 */
	public static boolean isValidDate(
			final String theString,
			final String thePattern
			) {
        return isValidDate(theString, getFormatter(thePattern));
    }

	/**
	 * Is string valid date?
	 *
	 * @param theString date string
	 * @param theFormatter formatter (null = standard formatter)
	 * @return valid date?
	 *
	 * @since 0.12.0
	 */
	public static boolean isValidDate(
			final String theString,
			final DateTimeFormatter theFormatter
			) {
        return parseDate(theString, theFormatter) != null;
    }


	/**
	 * Parse time with standard pattern/formatter.
	 *
	 * @param theString time string
	 * @return time
	 *
	 * @since 0.12.0
	 */
	public static LocalTime parseTime(
			final String theString
			) {
		return parseTime(theString, getFormatter(null));
    }

	/**
	 * Parse time with given pattern.
	 *
	 * @param theString time string
	 * @param thePattern pattern (null = standard pattern)
	 * @return time
	 *
	 * @since 0.12.0
	 */
	public static LocalTime parseTime(
			final String theString,
			final String thePattern
			) {
		return parseTime(theString, getFormatter(thePattern));
    }

	/**
	 * Parse time with given formatter.
	 *
	 * @param theString time string
	 * @param theFormatter formatter (null = standard formatter)
	 * @return date
	 *
	 * @since 0.12.0
	 */
	public static LocalTime parseTime(
			final String theString,
			final DateTimeFormatter theFormatter
			) {

		if (theString == null) {
			return null;
		}

        try {
            return LocalTime.parse(theString, (theFormatter == null) ? STANDARD_FORMATTERS.get(LocalTime.class) : theFormatter);
        } catch (DateTimeParseException e) {
            return null;
        }
    }

	/**
	 * Is string valid time?
	 *
	 * @param theString time string
	 * @return valid time?
	 *
	 * @since 0.12.0
	 */
	public static boolean isValidTime(
			final String theString
			) {
        return isValidTime(theString, getFormatter(null));
    }

	/**
	 * Is string valid time?
	 *
	 * @param theString time string
	 * @param thePattern pattern (null = standard pattern)
	 * @return valid time?
	 *
	 * @since 0.12.0
	 */
	public static boolean isValidTime(
			final String theString,
			final String thePattern
			) {
        return isValidTime(theString, getFormatter(thePattern));
    }

	/**
	 * Is string valid time?
	 *
	 * @param theString date string
	 * @param theFormatter formatter (null = standard formatter)
	 * @return valid time?
	 *
	 * @since 0.12.0
	 */
	public static boolean isValidTime(
			final String theString,
			final DateTimeFormatter theFormatter
			) {
        return parseTime(theString, theFormatter) != null;
    }


	/**
	 * Convert text to {@link LocalDateTime}.
	 *
	 * @param theString text to convert
	 * @return datetime (null if conversion was not successful)
	 *
	 * @since 0.9.1
	 */
	public static LocalDateTime parseDateTime(
			final String theString
			) {
		return parseDateTime(theString, getFormatter(null));
	}

	/**
	 * Convert text to {@link LocalDateTime} trying pattern and then standard patterns.
	 *
	 * @param theString text to convert
	 * @param thePattern pattern
	 * @return datetime (null if conversion was not successful)
	 *
	 * @since 0.9.7
	 */
	public static LocalDateTime parseDateTime(
			final String theString,
			final String thePattern
			) {
		return parseDateTime(theString, getFormatter(thePattern));
	}

	/**
	 * Convert text to {@link LocalDateTime} trying formatter and then standard formatters.
	 *
	 * TODO this is hacked really badly, because I can't grasp formatters fully.
	 *
	 * @param theString text to convert
	 * @param theFormatter formatter
	 * @return datetime (null if conversion was not successful)
	 *
	 * @since 0.12.0
	 */
	public static LocalDateTime parseDateTime(
			final String theString,
			final DateTimeFormatter theFormatter
			) {

		if (theString == null) {
			return null;
		}

		// if a formatter is given, use it and do not try other formatters
		if (theFormatter != null) {
			try {
				return LocalDateTime.parse(theString, theFormatter);
			} catch (DateTimeParseException e) {
				return null;
			}
		}

		List<DateTimeFormatter> lstFormatters = Arrays.asList(
						STANDARD_FORMATTERS.get(LocalDateTime.class),
						new DateTimeFormatterBuilder()
								.appendPattern(String.format("%s[, %s]", DATE_PATTERN, TIME_PATTERN))
								.parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
								.parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
								.parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
								.toFormatter(),
						new DateTimeFormatterBuilder()
								.appendPattern(String.format("[%s, ]%s", DATE_PATTERN, TIME_PATTERN))
								.parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
								.parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
								.parseDefaulting(ChronoField.YEAR, 2000)
								.toFormatter(),
						DateTimeFormatter.ISO_LOCAL_DATE_TIME,
						DateTimeFormatter.ofPattern("d.M.yyyy HH:mm:ss"),
						new DateTimeFormatterBuilder()
								.appendPattern("d.M.yyyy[ HH:mm:ss]")
								.parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
								.parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
								.parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
								.toFormatter(),
						new DateTimeFormatterBuilder()
								.appendPattern("[d.M.yyyy ]HH:mm:ss")
								.parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
								.parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
								.parseDefaulting(ChronoField.YEAR, 2000)
								.toFormatter(),
						DateTimeFormatter.ISO_OFFSET_DATE_TIME
				);

		for (DateTimeFormatter fmtFormatter : lstFormatters) {

			try {
				return LocalDateTime.parse(theString, fmtFormatter);
			} catch (DateTimeParseException e) {
				// ignore
			}

		}

		return null;

	}

	/**
	 * Convert {@link LocalDateTime} to {@link Date}.
	 *
	 * @param theDateTime datetime
	 * @return date
	 */
	public static Date toDate(
			final LocalDateTime theDateTime
			) {
		return Date.from(theDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * Convert {@link LocalDate} to {@link Date}.
	 *
	 * @param theDate date
	 * @return date
	 *
	 * @since 0.11.0
	 */
	public static Date toDate(
			final LocalDate theDate
			) {
		return Date.from(theDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}


	/**
	 * Returns date converter e.g. for date pickers with standard pattern.
	 *
	 * @return date converter
	 *
	 * @since 0.11.0
	 */
	public static StringConverter<LocalDate> getDateConverter() {
		return getDateConverter(getFormatter(null));
	}

	/**
	 * Returns date converter e.g. for date pickers.
	 *
	 * @param thePattern pattern (null = standard pattern)
	 * @return date converter
	 *
	 * @since 0.11.0
	 */
	public static StringConverter<LocalDate> getDateConverter(
			final String thePattern
			) {
		return getDateConverter(getFormatter(thePattern));
	}

	/**
	 * Returns date converter e.g. for date pickers.
	 *
	 * @param theFormatter formatter (null = standard formatter)
	 * @return date converter
	 *
	 * @since 0.12.0
	 */
	public static StringConverter<LocalDate> getDateConverter(
			final DateTimeFormatter theFormatter
			) {

		return new StringConverter<>() {

			@Override
			public String toString(LocalDate date) {
				if (date == null) {
					return "";
				}
				return DateTimeUtils.formatDate(date, theFormatter);
			}

			@Override
			public LocalDate fromString(String string) {
				return DateTimeUtils.parseDate(string, theFormatter);
			}

		};

	}

}

/* EOF */
