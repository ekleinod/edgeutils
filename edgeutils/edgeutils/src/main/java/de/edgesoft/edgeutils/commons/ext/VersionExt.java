package de.edgesoft.edgeutils.commons.ext;

import java.util.List;

import de.edgesoft.edgeutils.collections.CollectionHelper;
import de.edgesoft.edgeutils.commons.Additional;
import de.edgesoft.edgeutils.commons.AdditionalType;
import de.edgesoft.edgeutils.commons.ObjectFactory;
import de.edgesoft.edgeutils.commons.Version;

/**
 * Helper and convenience methods for {@link Version}.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.4
 */
public class VersionExt extends Version {

	/**
	 * Default constructor.
	 */
	public VersionExt() {
		super();
	}

	/**
	 * Constructor, setting version in one call, version as string.
	 *
	 * <ul>
	 * <li>"2"</li>
	 * <li>"2.0"</li>
	 * <li>"2.0.1"</li>
	 * <li>"2 pre 2"</li>
	 * <li>"2.0 alpha 1"</li>
	 * <li>"2.0.1 beta 5"</li>
	 * </ul>
	 *
	 * @param theVersion version
	 */
	public VersionExt(String theVersion) {
		super();

		setMajor(0);

		if ((theVersion == null) || theVersion.trim().isEmpty()) {
			return;
		}

		String sVersion = theVersion.trim();

		int index = 1;
		for (AdditionalType theAdditionalType : AdditionalType.values()) {
			List<String> lstTemp = CollectionHelper.fromCSVString(sVersion, theAdditionalType.value());
			if (lstTemp.size() > index) {
				Additional addType = new ObjectFactory().createAdditional();
				addType.setType(theAdditionalType);
				addType.setValue(Integer.parseInt(lstTemp.get(index)));
				setAdditional(addType);
				sVersion = lstTemp.get(0);
			}
		}

		List<String> lstTemp = CollectionHelper.fromCSVString(sVersion, ".");

		index = 0;
		if (lstTemp.size() > index) {
			setMajor(Integer.parseInt(lstTemp.get(index)));
		}

		index = 1;
		if (lstTemp.size() > index) {
			setMinor(Integer.parseInt(lstTemp.get(index)));
		}

		index = 2;
		if (lstTemp.size() > index) {
			setPatch(Integer.parseInt(lstTemp.get(index)));
		}

	}

	/**
	 * Creates string representation of version.
	 *
	 * @return string representation of version
	 * @since 0.6.0
	 */
	@Override
	public String toString() {
		StringBuilder sbReturn = new StringBuilder();

		sbReturn.append(getMajor());

		if (getMinor() != null) {
			sbReturn.append(".");
			sbReturn.append(getMinor());

			if (getPatch() != null) {
				sbReturn.append(".");
				sbReturn.append(getPatch());
			}
		}

		if (getAdditional() != null) {
			sbReturn.append(" ");
			sbReturn.append(getAdditional().getType().value());
			sbReturn.append(" ");
			sbReturn.append(getAdditional().getValue());
		}

		return sbReturn.toString();
	}

}

/* EOF */
