package de.edgesoft.edgeutils.xchart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Stroke;

import org.knowm.xchart.style.AbstractBaseTheme;
import org.knowm.xchart.style.MatlabTheme;
import org.knowm.xchart.style.PieStyler.AnnotationType;
import org.knowm.xchart.style.Theme;
import org.knowm.xchart.style.colors.ChartColor;

/**
 * Class providing base xchart {@link Theme}.
 *
 * Based on {@link MatlabTheme}.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * <p>edgeutils is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.10.0
 */
public class BaseTheme extends AbstractBaseTheme {

	/**
	 * Preferred font for charts.
	 */
	private static final String FONTNAME = "Droid Sans";

	@Override
	public Font getBaseFont() {
		return new Font(getFontName(), Font.PLAIN, 10);
	}

	@Override
	public Color[] getSeriesColors() {
		return new BaseSeriesColors().getSeriesColors();
	}

	// Chart Title ///////////////////////////////

	@Override
	public boolean isChartTitleBoxVisible() {
		return false;
	}

	// Chart Axes ///////////////////////////////

	@Override
	public Font getAxisTitleFont() {
		return getBaseFont().deriveFont(12f);
	}

	@Override
	public boolean isAxisTicksLineVisible() {
		return false;
	}

	@Override
	public boolean isAxisTicksMarksVisible() {
		return false;
	}

	@Override
	public Color getPlotBorderColor() {
		return ChartColor.getAWTColor(ChartColor.BLACK);
	}

	@Override
	public boolean isPlotBorderVisible() {
		return false;
	}

	@Override
	public Color getPlotGridLinesColor() {
		return ChartColor.getAWTColor(ChartColor.BLACK);
	}

	@Override
	public Stroke getPlotGridLinesStroke() {
		return new BasicStroke(0.25f, BasicStroke.CAP_BUTT, BasicStroke.CAP_ROUND, 10.0f, new float[] { 1.0f, 2.0f },
				0.0f);
	}

	@Override
	public double getPlotContentSize() {
		return .9;
	}

	// Pie Charts ///////////////////////////////

	@Override
	public double getAnnotationDistance() {
		return .82;
	}

	@Override
	public AnnotationType getAnnotationType() {
		return AnnotationType.Value;
	}

	@Override
	public boolean isSumVisible() {
		return true;
	}

	/**
	 * Returns font name of available font.
	 *
	 * @return font name
	 */
	public static String getFontName() {

		for (Font font : GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts()) {
			if (FONTNAME.equalsIgnoreCase(font.getFontName())) {
				return FONTNAME;
			}
		}

		return "SansSerif";

	}

}

/* EOF */
