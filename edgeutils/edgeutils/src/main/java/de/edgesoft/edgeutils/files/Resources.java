package de.edgesoft.edgeutils.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.logging.log4j.Logger;

import de.edgesoft.edgeutils.javafx.SVGImageTranscoder;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.image.Image;

/**
 * Resources helper.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.12.0
 */
public class Resources {

	/**
	 * Path for resources.
	 */
	public static final String PATH_RESOURCES = "resources";

	/**
	 * Path for views.
	 */
	public static final String PATH_VIEW = "view";

	/**
	 * Size for default size.
	 */
	public static final int USE_DEFAULT_SIZE = -1;


	/**
	 * Class providing resource access.
	 */
	private static Class<? extends Object> clsProvider = null;

	/**
	 * Logger.
	 */
	private static Logger logger = null;

	/**
	 * Image scaling factor.
	 */
	private static Optional<Double> dblScalingFactor = Optional.empty();

	/**
	 * Default image width.
	 */
	private static Optional<Integer> iDefaultWidth = Optional.empty();

	/**
	 * Project properties singleton.
	 */
	private static Properties prpProject = null;

	/**
	 * Initializes class.
	 *
	 * @param theProvider providing class
	 * @param theLogger logger
	 */
	public static void init(
			final Class<? extends Object> theProvider,
			final Logger theLogger
			) {

		clsProvider = theProvider;
		logger = theLogger;

	}

	/**
	 * Initializes class.
	 *
	 * @param theProvider providing class
	 * @param theLogger logger
	 * @param theScalingFactor scaling factor
	 * @param theDefaultWidth default width
	 */
	public static void init(
			final Class<? extends Object> theProvider,
			final Logger theLogger,
			final Double theScalingFactor,
			final Integer theDefaultWidth
			) {

		init(theProvider, theLogger);
		setScalingFactor(theScalingFactor);
		setDefaultWidth(theDefaultWidth);

	}

	/**
	 * Sets scaling factor.
	 *
	 * @param theScalingFactor scaling factor
	 */
	public static void setScalingFactor(
			final Double theScalingFactor
			) {

		dblScalingFactor = Optional.of(theScalingFactor);

	}

	/**
	 * Sets default image width.
	 *
	 * @param theDefaultWidth default width
	 */
	public static void setDefaultWidth(
			final Integer theDefaultWidth
			) {

		iDefaultWidth = Optional.of(theDefaultWidth);

	}

	/**
	 * Returns scaling factor (1 if none given).
	 *
	 * @return scaling factor
	 */
	public static Double getScalingFactor() {
		return dblScalingFactor.orElse(1.0);
	}

	/**
	 * Returns default width ({@link Prefs#SIZE_BUTTON} if none given).
	 *
	 * @return default width
	 */
	public static Integer getDefaultWidth() {
		return iDefaultWidth.orElse(Prefs.SIZE_BUTTON);
	}

	/**
	 * Returns providing class, asserts not null before returning class.
	 *
	 * @return providing class
	 */
	private static Class<? extends Object> getProvider() {

		assert (clsProvider != null) : "Providing class must not be null";

		return clsProvider;

	}

	/**
	 * Returns logger, asserts not null before returning class.
	 *
	 * @return logger
	 */
	public static Logger getLogger() {

		assert (logger != null) : "Logger must not be null";

		return logger;

	}

	/**
	 * Loads image from resources.
	 *
	 * @param theImagePath image path
	 * @return loaded image
	 */
	public static Image loadImage(
			final String theImagePath
			) {
		return loadImage(theImagePath, USE_DEFAULT_SIZE);
	}

	/**
	 * Loads image from resources, setting width (=height).
	 *
	 * @param theImagePath image path
	 * @param theWidth image width (=height)
	 *
	 * @return loaded image (null if an error occurred)
	 */
	public static Image loadImage(
			final String theImagePath,
			final int theWidth
			) {
		return loadImage(theImagePath, theWidth, theWidth);
	}

	/**
	 * Loads image from resources, setting width and height.
	 *
	 * @param theImagePath image path
	 * @param theWidth image width
	 * @param theHeight image height
	 *
	 * @return loaded image (null if an error occurred)
	 */
	public static Image loadImage(
			final String theImagePath,
			final int theWidth,
			final int theHeight
			) {

		Image imgReturn = null;

		if (theImagePath.endsWith(".svg")) {
			
			try (InputStream file = getProvider().getResourceAsStream(String.format("%s/%s", PATH_RESOURCES, theImagePath))) {
				
				if (file == null) {
					throw new FileNotFoundException(String.format("%s/%s", PATH_RESOURCES, theImagePath));
				}
				
				int iWidth = (int) Math.round(((theWidth > 0) ? theWidth : getDefaultWidth()) * getScalingFactor());
				int iHeight = (int) Math.round(((theHeight > 0) ? theHeight : getDefaultWidth()) * getScalingFactor());

				try {
					
					SVGImageTranscoder trans = SVGImageTranscoder.createSVGImageTranscoder(iWidth, iHeight);
					trans.transcode(new TranscoderInput(file), null);
					imgReturn = trans.getFXImage();
					
				} catch (TranscoderException e) {
					getLogger().catching(e);
				}
				
			} catch (IOException e) {
				getLogger().catching(e);
			}

		} else {
			
			imgReturn = new Image(getProvider().getResourceAsStream(String.format("%s/%s", PATH_RESOURCES, theImagePath)));

		}
		
		return imgReturn;

	}

	/**
	 * Loads fxml parent from resources.
	 *
	 * @param theParentName parent file name
	 * @return loaded parent
	 */
	public static Map.Entry<Parent, FXMLLoader> loadNode(
			final String theParentName
			) {
		
		Map.Entry<Object, FXMLLoader> fxmlObject = loadFXMLObject(theParentName);
		
		if (fxmlObject == null) {
			return null;
		}
		
		return new AbstractMap.SimpleImmutableEntry<>((Parent) fxmlObject.getKey(), fxmlObject.getValue());

	}

	/**
	 * Loads fxml object from resources.
	 *
	 * @param theFXMLName fxml file name
	 * @return loaded node
	 */
	public static Map.Entry<Object, FXMLLoader> loadFXMLObject(
			final String theFXMLName
			) {

		try {

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getProvider().getResource(String.format("%s/%s.fxml", PATH_VIEW, theFXMLName)));
			return new AbstractMap.SimpleImmutableEntry<>(loader.load(), loader);

		} catch (IOException e) {
			getLogger().catching(e);
			return null;
		}

	}

	/**
	 * Loads file from resources.
	 *
	 * @param theFileName file name
	 * @return loaded file as string
	 */
	public static String loadFile(final String theFileName) {

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(getProvider().getResourceAsStream(String.format("%s/%s", PATH_RESOURCES, theFileName))))) {

			StringBuilder sbReturn = new StringBuilder();
			String sLine = null;

			while ((sLine = reader.readLine()) != null) {
				sbReturn.append(sLine);
			}

			return sbReturn.toString();

		} catch (Exception e) {
			getLogger().catching(e);
			return "";
		}

	}

	/**
	 * Loads project properties from resources.
	 *
	 * @return project properties
	 */
	public static Properties getProjectProperties() {
		if (prpProject == null) {
			prpProject = loadProperties(String.format("%s/%s", PATH_RESOURCES, "project.properties"));
		}
		return prpProject;
    }

	/**
	 * Loads properties from resources.
	 *
	 * @param theFileName pane name
	 * @return loaded file as properties
	 */
	public static Properties loadProperties(final String theFileName) {

		Properties prpReturn = new Properties();

		try {

			prpReturn.load(getProvider().getResourceAsStream(theFileName));

		} catch (Exception e) {
			getLogger().catching(e);
		}

		return prpReturn;

    }

	/**
	 * Returns URI that holds in jars as well.
	 *
	 * @param theFileName file name
	 * @return URI in usable form
	 */
	public static String getExternalURI(
			final String theFileName
			) {

		return getProvider().getResource(theFileName).toExternalForm();

    }

	/**
	 * Returns filelist of resource path, either from file system or from jar.
	 *
	 * This is unexpectedly difficult, I used: http://www.uofr.net/~greg/java/get-resource-listing.html
	 *
	 * @param theResourcePath resource path
	 * @param theFileExtension file extension to filter the files for (null for all files)
	 * @return list of filenames in path
	 * @throws IOException if an error occurred
	 * 
	 * @since 0.13.0
	 */
	public static List<String> getSortedFileList(
			final String theResourcePath,
			final String theFileExtension
			) throws IOException {

		try {

			URL dirURL = getProvider().getResource(theResourcePath);

			if (dirURL == null) {
				throw new FileNotFoundException(String.format("resource not found: %s", theResourcePath));
			}

			// replace from here with code below if enable preview works
			Stream<String> stmReturn = null;

			if (dirURL.getProtocol().equals("file")) {

				stmReturn = Arrays.asList(new File(dirURL.toURI()).list()).stream();

			}

			if (dirURL.getProtocol().equals("jar")) {

				String sJarPath = dirURL.getPath().substring("file:".length(), dirURL.getPath().indexOf("!"));
				String sResourcePath = dirURL.getPath().substring(dirURL.getPath().indexOf("!") + 2);

		        try (JarFile jfJarFile = new JarFile(URLDecoder.decode(sJarPath, "UTF-8"))) {

		        	stmReturn = Collections.list(jfJarFile.entries())
		        			.stream()
		        			.map(JarEntry::getName)
		        			.filter(filename -> filename.startsWith(sResourcePath))
		        			.map(filename -> filename.substring(sResourcePath.length()));

		        }

			}

			if (stmReturn == null) {
				throw new IOException(String.format("Cannot list files for URL %s, because protocol %s is not supported.", dirURL, dirURL.getProtocol()));
			}

			// enable-preview generates an error in maven, therefore the following code cannot be used for now
//			Stream<String> stmReturn = switch (dirURL.getProtocol()) {
//
//				case "file" -> Arrays.asList(new File(dirURL.toURI()).list()).stream();
//
//				case "jar" -> {
//
//					String sJarPath = dirURL.getPath().substring("file:".length(), dirURL.getPath().indexOf("!"));
//					String sResourcePath = dirURL.getPath().substring(dirURL.getPath().indexOf("!") + 2);
//					Stream<String> stmTemp = null;
//
//			        try (JarFile jfJarFile = new JarFile(URLDecoder.decode(sJarPath, "UTF-8"))) {
//
//			        	stmTemp = Collections.list(jfJarFile.entries())
//			        			.stream()
//			        			.map(JarEntry::getName)
//			        			.filter(filename -> filename.startsWith(sResourcePath))
//			        			.map(filename -> filename.substring(sResourcePath.length()));
//
//			        }
//
//			        break stmTemp;
//
//				}
//
//				default -> throw new IOException(String.format("Cannot list files for URL %s, because protocol %s is not supported.", dirURL, dirURL.getProtocol()));
//
//			};

			return stmReturn
					.distinct()
	        		.filter(filename -> (theFileExtension == null) ? true : filename.endsWith(theFileExtension))
					.sorted()
	        		.collect(Collectors.toList());


		} catch (IOException | URISyntaxException e) {
			throw new IOException(e);
		}

	}

}

/* EOF */
