package de.edgesoft.edgeutils.javafx;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;


/**
 * Utilities for {@link TreeView}.
 *
 * <p>When trying Kotlin, all static methods are candidates for
 * extending the button class.</p>
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.12.0
 */
public class TreeViewUtils {

	/**
	 * Returns list of checked {@link CheckBoxTreeItem}s.
	 *
	 * @param <T> content of tree items
	 * @param theTreeView button to adapt
	 * @return list of checked items in the tree view, empty if there are none
	 */
	public static <T> List<TreeItem<T>> getCheckedCheckBoxTreeItems(
			TreeView<T> theTreeView
			) {

		List<TreeItem<T>> lstReturn = new ArrayList<>();

		CheckBoxTreeItem<T> itmRoot = (CheckBoxTreeItem<T>) theTreeView.getRoot();
		if (itmRoot.isSelected()) {
			lstReturn.add(itmRoot);
		}
		lstReturn.addAll(getCheckedChildren(itmRoot));

		return lstReturn;

	}

	/**
	 * Returns list of checked {@link CheckBoxTreeItem} children of the given {@link TreeItem}.
	 *
	 * @param <T> content of tree items
	 * @param theTreeItem the tree item
	 * @return list of checked children, empty if none
	 */
	private static <T> List<TreeItem<T>> getCheckedChildren(
			TreeItem<T> theTreeItem
			) {

		List<TreeItem<T>> lstReturn = new ArrayList<>();

		theTreeItem.getChildren().forEach(child -> {

			if (((CheckBoxTreeItem<T>) child).isSelected()) {
				lstReturn.add(child);
			}
			lstReturn.addAll(getCheckedChildren(child));

		});

		return lstReturn;

	}

}

/* EOF */
