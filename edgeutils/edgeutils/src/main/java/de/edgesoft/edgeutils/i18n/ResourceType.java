
package de.edgesoft.edgeutils.i18n;

/**
 * Resource types.
 *
 * For enums I use the coding style of jaxb, so there will be no inconsistencies.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * <p>edgeutils is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.13.0
 */
public enum ResourceType {

	ACCELERATOR,
	ICON,
	PATTERN,
	PROMPTTEXT,
	TEXT,
	TOOLTIP
	;

	private final String value;

	ResourceType() {
		value = name().toLowerCase();
	}

	public String value() {
		return value;
	}

	public static ResourceType fromValue(String v) {
		for (ResourceType c: ResourceType.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}

/* EOF */
