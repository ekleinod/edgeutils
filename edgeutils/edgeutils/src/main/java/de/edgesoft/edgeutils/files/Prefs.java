package de.edgesoft.edgeutils.files;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;

/**
 * Preferences.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * <p>edgeutils is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.12.0
 */
public class Prefs {

	/**
	 * Button size - normal.
	 */
	public final static int SIZE_BUTTON = 24;

	/**
	 * Button size - small.
	 */
	public final static int SIZE_BUTTON_SMALL = 16;

	/**
	 * Alert image size.
	 */
	public final static int SIZE_ALERT = 48;
	
	/**
	 * Flag size.
	 */
	public final static int SIZE_FLAG = 20;


	/**
	 * Preferences object (singleton).
	 */
	private static Preferences preferences = null;

	/**
	 * Initializes class.
	 *
	 * @param theProvider providing class
	 */
	public static void init(
			final Class<? extends Object> theProvider
			) {

		if (preferences == null) {
			preferences = Preferences.userNodeForPackage(theProvider);
		}

	}

	/**
	 * Returns preferences.
	 *
	 * @return preferences
	 */
	public static Preferences getPrefs() {

		assert (preferences != null) : "Prefs were not initialized.";

		return preferences;

	}

	/**
	 * Set preference value for text key.
	 *
	 * @param theKey text key
	 * @param theValue value
	 */
	public static void put(
			final String theKey,
			final String theValue
			) {
		put(theKey, theValue, null);
	}

	/**
	 * Set preference value for text key, unset if default value is used or if value is empty.
	 *
	 * @param theKey text key
	 * @param theValue value
	 * @param theDefaultValue default value
	 * 
	 * @since 0.15.0
	 */
	public static void put(
			final String theKey,
			final String theValue,
			final String theDefaultValue
			) {
		
		if (
				(theValue == null) || (theValue.isBlank()) ||
				((theDefaultValue != null) && (theValue.equals(theDefaultValue)))
		) {
			remove(theKey);
		} else {
			getPrefs().put(theKey, theValue);
		}
	}

	/**
	 * Get preference value for text key, default "".
	 *
	 * @param theKey text key
	 * @return value
	 */
	public static String get(
			final String theKey
			) {
		return get(theKey, "");
	}

	/**
	 * Get preference value for text key.
	 *
	 * @param theKey text key
	 * @param theDefault default value
	 * @return value
	 */
	public static String get(
			final String theKey,
			final String theDefault
			) {
		return getPrefs().get(theKey, theDefault);
	}

	/**
	 * Exports preferences.
	 *
	 * @param theStream output stream
	 *
	 * @throws BackingStoreException
	 * @throws IOException
	 */
	public static void exportPrefs(
			final OutputStream theStream
			) throws IOException, BackingStoreException {
		getPrefs().exportNode(theStream);
	}

	/**
	 * Imports preferences.
	 *
	 * @param theStream input stream
	 *
	 * @throws IOException
	 * @throws InvalidPreferencesFormatException
	 * @throws BackingStoreException
	 */
	public static void importPrefs(
			final InputStream theStream
			) throws IOException, InvalidPreferencesFormatException, BackingStoreException {
		getPrefs().clear();
		Preferences.importPreferences(theStream);
	}

	/**
	 * Returns preferences map.
	 *
	 * @return preferences map
	 *
	 * @throws BackingStoreException
	 */
	public static Map<String, Object> getPrefsMap() throws BackingStoreException {

		Map<String, Object> mapReturn = new HashMap<>();

		for (String theKey : getPrefs().keys()) {
			mapReturn.put(theKey, get(theKey));
		}

		return mapReturn;
	}

	/**
	 * Remove key from preferences.
	 *
	 * @param theKey key
	 * 
	 * @since 0.15.0
	 */
	public static void remove(
			final String theKey
			) {
		
		getPrefs().remove(theKey);
	}

}

/* EOF */
