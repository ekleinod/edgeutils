package de.edgesoft.edgeutils.javafx;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.logging.log4j.util.Strings;

import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.edgeutils.i18n.ResourceType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBoxBase;
import javafx.scene.control.Control;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.image.ImageView;


/**
 * Utilities for {@link Control}s.
 *
 * <p>See comment for {@link MenuItemUtils} and {@link TabUtils}.</p>
 * 
 * <p>When trying Kotlin, all static methods are candidates for
 * extending the button class.</p>
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.13.0
 */
public class ControlUtils {

	/**
	 * Variable name prefixes.
	 */
	public static final Map<Class<? extends Control>, String> PREFIXES = Map.of(
			Button.class, "btn",
			SplitMenuButton.class, "btn",
			CheckBox.class, "chk",
			Label.class, "lbl",
			DatePicker.class, "pck",
			EdgeDatePicker.class, "pck",
			RadioButton.class, "rad",
			TextField.class, "txt",
			TextInputControl.class, "txt",
			TimeTextField.class, "txt"
			);

	
	/**
	 * Fills control with graphic (default size), text, and tooltip.
	 * 
	 * One could use {@link Labeled} instead of {@link Control} but my hope is, that
	 * {@link MenuItem}s someday will be at least {@link Control}s and thus
	 * can be filled within this class.
	 * 
	 * @param theControl control to fill
	 * @param theController view controller
	 * @param theResourceTypes resource types
	 */
	public static void fillViewControl(
			Control theControl,
    		final Object theController,
    		final ResourceType ... theResourceTypes
			) {
		fillViewControl(theControl, null, theController, theResourceTypes);
	}

	/**
	 * Fills control with graphic (default size), text, and tooltip.
	 * 
	 * One could use {@link Labeled} instead of {@link Control} but my hope is, that
	 * {@link MenuItem}s someday will be at least {@link Control}s and thus
	 * can be filled within this class.
	 * 
	 * @param theControl control to fill
	 * @param theAdditionalId additional id (null for none)
	 * @param theController view controller
	 * @param theResourceTypes resource types
	 */
	public static void fillViewControl(
			Control theControl,
			final String theAdditionalId,
    		final Object theController,
    		final ResourceType ... theResourceTypes
			) {
		fillViewControl(theControl, theAdditionalId, theController, Resources.USE_DEFAULT_SIZE, theResourceTypes);
	}

	/**
	 * Fills control with graphic (given size), text, and tooltip.
	 * 
	 * @param theControl control to fill
	 * @param theController view controller
	 * @param theSize size of the icon
	 * @param theResourceTypes resource types
	 */
	public static void fillViewControl(
			Control theControl,
    		final Object theController,
    		final int theSize,
    		final ResourceType ... theResourceTypes
			) {
		fillViewControl(theControl, null, theController, theSize, theResourceTypes);
	}
		
	/**
	 * Fills control with graphic (given size), text, and tooltip.
	 * 
	 * One could use {@link Labeled} instead of {@link Control} but my hope is, that
	 * {@link MenuItem}s someday will be at least {@link Control}s and thus
	 * can be filled within this class.
	 * 
	 * @param theControl control to fill
	 * @param theAdditionalId additional id (null for none)
	 * @param theController view controller
	 * @param theSize size of the icon
	 * @param theResourceTypes resource types (optional)
	 */
	public static void fillViewControl(
			Control theControl,
			final String theAdditionalId,
    		final Object theController,
    		final int theSize,
    		final ResourceType ... theResourceTypes
			) {
		
		Objects.requireNonNull(theControl, "control must not be null.");
		
		List<ResourceType> lstResourceTypes = (theResourceTypes.length == 0) ?
				Arrays.asList(ResourceType.values()) :
				Arrays.asList(theResourceTypes);
				
		String sID = getIdWithoutPrefix(theControl, theAdditionalId);
		
		if (theControl instanceof Labeled) {
			
			if (lstResourceTypes.contains(ResourceType.TEXT)) {
				I18N.getViewNodeText(theController, sID, ResourceType.TEXT).ifPresent(it -> {
					((Labeled) theControl).setText(it);
					((Labeled) theControl).setMnemonicParsing(true);
				});
			}
			
			if (lstResourceTypes.contains(ResourceType.ICON)) {
				I18N.getViewNodeText(theController, sID, ResourceType.ICON).ifPresent(it -> ((Labeled) theControl).setGraphic(new ImageView(Resources.loadImage(it, theSize))));
			}
			
		}
		
		if (theControl instanceof ButtonBase) {
			
			if (lstResourceTypes.contains(ResourceType.TOOLTIP) || I18N.containsKey(I18N.getViewNodeKey(theController, sID, ResourceType.TEXT))) {
				
				theControl.setTooltip(TooltipUtils.createTooltip(
						I18N.getViewNodeText(theController, sID, lstResourceTypes.contains(ResourceType.TOOLTIP) ? ResourceType.TOOLTIP : ResourceType.TEXT).get(),
						I18N.containsKey(I18N.getViewNodeKey(theController, sID, ResourceType.ACCELERATOR)) ? 
								I18N.getViewNodeText(theController, sID, ResourceType.ACCELERATOR).get() :
								null
						));
				
			}
			
		}
		
		if (theControl instanceof ComboBoxBase<?>) {
			
			if (lstResourceTypes.contains(ResourceType.PROMPTTEXT)) {
				I18N.getViewNodeText(theController, sID, ResourceType.PROMPTTEXT).ifPresent(it -> {
					((ComboBoxBase<?>) theControl).setPromptText(it);
				});
			}
			
		}
		
		if (theControl instanceof TextInputControl) {
			
			if (lstResourceTypes.contains(ResourceType.PROMPTTEXT)) {
				I18N.getViewNodeText(theController, sID, ResourceType.PROMPTTEXT).ifPresent(it -> {
					((TextInputControl) theControl).setPromptText(it);
				});
			}
			
		}
		
	}
	
	/**
	 * Returns id without prefix from control with ID.
	 * 
	 * @param theControl control with id
	 * @return id without prefix
	 */
	public static String getIdWithoutPrefix(
			final Control theControl
			) {
		return getIdWithoutPrefix(theControl, null);		
	}

	/**
	 * Returns id without prefix from control with ID.
	 * 
	 * @param theControl control with id
	 * @param theAdditionalId additional id (null for none)
	 * @return id without prefix
	 */
	public static String getIdWithoutPrefix(
			final Control theControl,
			final String theAdditionalId
			) {
		
		Objects.requireNonNull(theControl, "control must not be null.");
		
		String sPrefix = PREFIXES.get(theControl.getClass());
		
		String sID = removePrefix(theControl.getId(), sPrefix);
		
		if (Strings.isNotBlank(theAdditionalId)) {
			sID = String.join(I18N.DELIMITER, sID, theAdditionalId);
		}
		
		return sID;
		
	}

	/**
	 * Fills controls with specific resource type.
	 * 
	 * @param theResourceType resource type
	 * @param theController view controller
	 * @param theControls control to fill
	 */
	public static void fillViewControls(
			final ResourceType theResourceType,
    		final Object theController,
    		Control ... theControls
			) {
		
		Arrays.asList(theControls).stream().forEach(it -> fillViewControl(it, theController, theResourceType));
		
	}

	/**
	 * Removes prefix from ID.
	 * 
	 * @param theID id with prefix
	 * @param thePrefix prefix
	 * @return id without prefix
	 */
	public static String removePrefix(
			final String theID,
			final String thePrefix
			) {
		return (theID.startsWith(thePrefix)) ? theID.substring(thePrefix.length()) : theID;
	}

}

/* EOF */
