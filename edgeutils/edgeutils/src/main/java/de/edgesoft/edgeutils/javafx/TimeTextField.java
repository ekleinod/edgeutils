package de.edgesoft.edgeutils.javafx;

import java.time.LocalTime;

import de.edgesoft.edgeutils.datetime.DateTimeUtils;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.TextField;


/**
 * A text field for input of times {@link LocalTime}.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.13.0
 */
public class TimeTextField extends TextField {

	/**
	 * Returns input as time.
	 * 
	 * @return time
	 */
	public LocalTime getTime() {
		
		if (getText() == null) {
			return null;
		}
		
		return DateTimeUtils.parseTime(getText());
		
	}
	
	/**
	 * Returns input as object property with time.
	 * 
	 * @return time
	 */
	public ObjectProperty<LocalTime> getTimeAsProperty() {
		
		if (getTime() == null) {
			return null;
		}
		
		return new SimpleObjectProperty<>(getTime());
		
	}
	
	/**
	 * Sets time.
	 * 
	 * @param theTime time to set
	 */
	public void setTime(final LocalTime theTime) {
		
		setText(DateTimeUtils.formatTime(theTime));
		
	}
	
	/**
	 * Sets time from object property.
	 * 
	 * @param theTime time to set
	 */
	public void setTimeFromProperty(final ObjectProperty<LocalTime> theTime) {
		
		setTime((theTime == null) ? null : theTime.getValue());
		
	}
	
}

/* EOF */
