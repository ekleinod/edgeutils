package de.edgesoft.edgeutils.javafx;

import org.apache.logging.log4j.util.Strings;

import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCombination;


/**
 * Utilities for {@link Tooltip}.
 *
 * <p>When trying Kotlin, all static methods are candidates for
 * extending the button class.</p>
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.13.0
 */
public class TooltipUtils {

	/**
	 * Tooltip token.
	 */
	public static final String TOOLTIP_TOKEN = "%s%s";

	/**
	 * Accelerator token.
	 */
	public static final String ACCELERATOR_TOKEN = " (%s)";

	
	/**
	 * Creates a tooltip with text.
	 *
	 * @param theTooltipText tooltip text
	 * @return tooltip
	 */
	public static Tooltip createTooltip(
			final String theTooltipText
			) {

		return createTooltip(theTooltipText, (String) null);

	}

	/**
	 * Creates a tooltip with text and key combination.
	 *
	 * @param theTooltipText tooltip text
	 * @param theKeyCombination key combination (null for none)
	 * @return tooltip
	 */
	public static Tooltip createTooltip(
			final String theTooltipText,
			final KeyCombination theKeyCombination
			) {

		return createTooltip(theTooltipText, (theKeyCombination == null) ? (String) null : theKeyCombination.getDisplayText());

	}

	/**
	 * Creates a tooltip with text and accelerator text.
	 *
	 * @param theTooltipText tooltip text
	 * @param theAcceleratorText accelerator text (null or empty for none)
	 * @return tooltip
	 */
	public static Tooltip createTooltip(
			final String theTooltipText,
			final String theAcceleratorText
			) {

		return new Tooltip(
				LabeledUtils.removeMnemonicMarker(
						String.format(TOOLTIP_TOKEN,
								theTooltipText,
								Strings.isBlank(theAcceleratorText) ? "" : String.format(ACCELERATOR_TOKEN, theAcceleratorText)
				)));

	}

}

/* EOF */
