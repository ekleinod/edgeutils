package de.edgesoft.edgeutils.javafx.controller;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.naming.OperationNotSupportedException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * Abstract controller with hyperlink abilities.
 *
 * <p>Useful for all controllers that need to follow a hyperlink or copy an URL.</p>
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.12.0
 */
public abstract class AbstractHyperlinkController {
	
	private static boolean FOLLOW_LINKS = false; 

	/**
	 * Follow an email link.
	 *
	 * <p>The method could be declared static but this prevents the FXML magic, thus the corresponding warning is suppressed.</p>
	 *
	 * @param theEvent action event
	 */
	@SuppressWarnings("static-method")
	@FXML
	public void handleEmailLinkAction(
			final ActionEvent theEvent
			) {

		if (FOLLOW_LINKS) {
			
			// this code should work, but it does not on Ubuntu, nobody knows, why (at least I don't)
			try {
				if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.MAIL)) {
					Desktop.getDesktop().mail(new URI(String.format("mailto:%s", ((Hyperlink) theEvent.getTarget()).getText())));
				} else {
					throw new OperationNotSupportedException("Desktop does not support web links.");
				}
			} catch (IOException | URISyntaxException | OperationNotSupportedException e) {
				e.printStackTrace();
			}
			
		} else {
			
	        showSorry(((Hyperlink) theEvent.getTarget()).getText());
			
		}

	}

	/**
	 * Follow a web link.
	 *
	 * <p>The method could be declared static but this prevents the FXML magic, thus the corresponding warning is suppressed.</p>
	 *
	 * @param theEvent action event
	 */
	@SuppressWarnings("static-method")
	@FXML
	public void handleWebLinkAction(
			final ActionEvent theEvent
			) {
		
		if (FOLLOW_LINKS) {
			
			// this code should work, but it does not on Ubuntu, nobody knows, why (at least I don't)
			try {
				if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.APP_OPEN_URI)) {
					Desktop.getDesktop().browse(new URI(((Hyperlink) theEvent.getTarget()).getText()));
				} else {
					throw new OperationNotSupportedException("Desktop does not support web links.");
				}
			} catch (IOException | URISyntaxException | OperationNotSupportedException e) {
				e.printStackTrace();
			}
			
		} else {
			
	        showSorry(((Hyperlink) theEvent.getTarget()).getText());
			
		}

	}

	/**
	 * Shows sorry alert.
	 * 
	 * @param theLink link to follow
	 */
	private static void showSorry(final String theLink) {
		
		Alert alert = new Alert(AlertType.INFORMATION);

		// set owning stage
		alert.initOwner(null);

		// display all text and resize to height
		alert.setResizable(true);
		alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
		alert.getDialogPane().setPrefWidth(400.0);

		// set texts
		alert.setTitle("Öffnen eines Links");
		alert.setHeaderText("Link kann nicht geöffnet werden");
		
		// set content node
		VBox boxContent = new VBox(5.0);
		Label lblSorry = new Label("Leider ist es nicht so einfach, einen Link zu öffnen, daher kann ich nur anbieten, durch Anklicken des Buttons neben dem Link, diesen in die Zwischenablage zu kopieren.\nTut mir leid.");
		lblSorry.setWrapText(true);
		boxContent.getChildren().add(lblSorry);
		boxContent.getChildren().add(new Separator(Orientation.HORIZONTAL));
		boxContent.getChildren().add(CopyTextPane.createInstance(theLink, true));
		alert.getDialogPane().setContent(boxContent);
		
		// show (and wait)
		alert.showAndWait();
		
	}
	
}

/* EOF */
