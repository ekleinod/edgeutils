package de.edgesoft.edgeutils.markup.reveal;




/**
 * Reveal (reveal.js) markup constants and methods.
 * 
 * <h3>Legal stuff</h3>
 * 
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 * 
 * <p>This file is part of edgeutils.</p>
 * 
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 * 
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 * 
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 * 
 * @author Ekkart Kleinod
 * @since 0.2
 */
public class RevealMarkup {
	
	/** slide start. */
	public final static String SLIDE_START = "<section>\n";
	
	/** slide end. */
	public final static String SLIDE_END = "</section>\n";
	
	/** heading token. */
	public final static String HEADING_TOKEN = "<h%1$d>%%s</h%1$d>\n\n";
	
	/** paragraph. */
	public final static String PARAGRAPH = "<p class=\"%s\">%s</p>\n";
	
	/** list start. */
	public final static String LIST_START = "<ol class=\"%s\">\n";
	
	/** list item start. */
	public final static String LIST_ITEM_START = "<li class=\"%s\">";
	
	/** list item end. */
	public final static String LIST_ITEM_END = "</li>\n";
	
	/** list end. */
	public final static String LIST_END = "</ol>\n";
	
	/** blockquote start. */
	public final static String BLOCKQUOTE_START = "<blockquote>\n";
	
	/** blockquote end. */
	public final static String BLOCKQUOTE_END = "</blockquote>\n";
	
	/**
	 * Returns heading token for given level.
	 * 
	 * @param iLevel level of heading
	 * @return heading token
	 */
	public static String getHeadingToken(int iLevel) {
		return String.format(HEADING_TOKEN, iLevel);		
	}
	
}

/* EOF */
