package de.edgesoft.edgeutils.xchart;

import java.util.HashMap;
import java.util.Map;

import org.knowm.xchart.BitmapEncoder.BitmapFormat;
import org.knowm.xchart.VectorGraphicsEncoder.VectorGraphicsFormat;

/**
 * Class providing encoder formats.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * <p>edgeutils is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.10.0
 */
public class EncoderFormats {

	/**
	 * Map of bitmap formats.
	 */
	private static Map<String, BitmapFormat> mapBitmapFormats = null;

	/**
	 * Map of vector formats.
	 */
	private static Map<String, VectorGraphicsFormat> mapVectorFormats = null;

	/**
	 * Returns bitmap formats.
	 *
	 * @return map of bitmap formats
	 */
	public static Map<String, BitmapFormat> BitmapFormats() {

		if (mapBitmapFormats == null) {

			mapBitmapFormats = new HashMap<>();

			for (BitmapFormat format : BitmapFormat.values()) {
				mapBitmapFormats.put(format.toString().toLowerCase(), format);
			}

		}

		return mapBitmapFormats;

	}

	/**
	 * Returns vector formats.
	 *
	 * @return map of vector formats
	 */
	public static Map<String, VectorGraphicsFormat> VectorFormats() {

		if (mapVectorFormats == null) {

			mapVectorFormats = new HashMap<>();

			for (VectorGraphicsFormat format : VectorGraphicsFormat.values()) {
				mapVectorFormats.put(format.toString().toLowerCase(), format);
			}

		}

		return mapVectorFormats;

	}

}

/* EOF */
