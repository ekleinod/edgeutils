package de.edgesoft.edgeutils.javafx;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

import javafx.scene.paint.Color;

/**
 * Unit test for {@link ColorUtils}.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.9.3
 */
@SuppressWarnings("static-method")
public class ColorUtilsTest {

	/**
	 * Tests formatWebHex.
	 */
	@Test
	public void testFormatWebHex() {

		assertEquals("#F0FFFF", ColorUtils.formatWebHex(Color.AZURE));
		assertEquals("#000000", ColorUtils.formatWebHex(Color.BLACK));
		assertEquals("#FDF5E6", ColorUtils.formatWebHex(Color.OLDLACE));
		assertEquals("#FFFFFF", ColorUtils.formatWebHex(Color.WHITE));

	}
	
}

/* EOF */
