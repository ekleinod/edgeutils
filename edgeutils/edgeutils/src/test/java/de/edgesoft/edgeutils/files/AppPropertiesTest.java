package de.edgesoft.edgeutils.files;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

/**
 * Unit test for AppProperties.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.5.0
 */
@SuppressWarnings("static-method")
public class AppPropertiesTest {

	/** File name default property file: */
	private static final String DEFAULT = "default.properties";

	/** File name app property file: */
	private static final String APP = "app.properties";

	/**
	 * Initialize property files.
	 * @throws Exception
	 */
	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
		FileAccess.setEncoding(StandardCharsets.ISO_8859_1);
		FileAccess.writeFile(Paths.get(DEFAULT), "color=blue\nperson=me\nname=täter");
		FileAccess.writeFile(Paths.get(APP), "color=red");
	}

	/**
	 * Delete property files.
	 * @throws Exception
	 */
	@AfterAll
	public static void tearDownAfterClass() throws Exception {
		Files.deleteIfExists(Paths.get(DEFAULT));
		Files.deleteIfExists(Paths.get(APP));
	}

	/**
	 * Tests null null.
	 */
	@Test
	public void testErrorNullNull() {

		try {
			Properties prpTest = AppProperties.getProperties((String) null, null, false);

			assertNotNull(prpTest);
			assertEquals(new Properties(), prpTest);
		} catch (IOException e) {
			fail(e.getMessage());
		}

	}

	/**
	 * Tests missing null.
	 */
	@Test
	@EnabledOnOs(OS.WINDOWS)
	public void testErrorMissingNullWin() {

		String sFilename = "missing.properties";

		Throwable exception = assertThrows(FileNotFoundException.class,
				() -> {
					AppProperties.getProperties(sFilename, null, false);
				});

		assertEquals(String.format("%s (%s)", sFilename, "Das System kann die angegebene Datei nicht finden"), exception.getMessage());

	}

	/**
	 * Tests missing null.
	 */
	@Test
	@DisabledOnOs(OS.WINDOWS)
	public void testErrorMissingNullNotWin() {

		String sFilename = "missing.properties";

		Throwable exception = assertThrows(FileNotFoundException.class,
				() -> {
					AppProperties.getProperties(sFilename, null, false);
				});

		assertEquals(String.format("%s (%s)", sFilename, "No such file or directory"), exception.getMessage());

	}

	/**
	 * Tests null missing.
	 */
	@Test
	@EnabledOnOs(OS.WINDOWS)
	public void testErrorNullMissingWin() {

		String sFilename = "missing.properties";

		Throwable exception = assertThrows(FileNotFoundException.class,
				() -> {
					AppProperties.getProperties((String) null, sFilename, false);
				});

		assertEquals(String.format("%s (%s)", sFilename, "Das System kann die angegebene Datei nicht finden"), exception.getMessage());

	}

	/**
	 * Tests null missing.
	 */
	@Test
	@DisabledOnOs(OS.WINDOWS)
	public void testErrorNullMissingNotWin() {

		String sFilename = "missing.properties";

		Throwable exception = assertThrows(FileNotFoundException.class,
				() -> {
					AppProperties.getProperties((String) null, sFilename, false);
				});

		assertEquals(String.format("%s (%s)", sFilename, "No such file or directory"), exception.getMessage());

	}

	/**
	 * Tests missing missing.
	 */
	@Test
	@EnabledOnOs(OS.WINDOWS)
	public void testErrorMissingMissingWin() {

		String sFilename = "missing.properties";

		Throwable exception = assertThrows(FileNotFoundException.class,
				() -> {
					AppProperties.getProperties(sFilename, sFilename, false);
				});

		assertEquals(String.format("%s (%s)", sFilename, "Das System kann die angegebene Datei nicht finden"), exception.getMessage());

	}

	/**
	 * Tests missing missing.
	 */
	@Test
	@DisabledOnOs(OS.WINDOWS)
	public void testErrorMissingMissingNotWin() {

		String sFilename = "missing.properties";

		Throwable exception = assertThrows(FileNotFoundException.class,
				() -> {
					AppProperties.getProperties(sFilename, sFilename, false);
				});

		assertEquals(String.format("%s (%s)", sFilename, "No such file or directory"), exception.getMessage());

	}

	/**
	 * Tests default.
	 */
	@Test
	public void testDefault() {

		Properties prpTest = null;

		try {
			prpTest = AppProperties.getProperties(DEFAULT, null, false);

			assertNotNull(prpTest);
			assertEquals(0, prpTest.size());
			assertEquals(Set.of("color", "person", "name"), new HashSet<>(Collections.list((Enumeration<String>) prpTest.propertyNames())));

			assertEquals("blue", prpTest.getProperty("color"));
			assertEquals("me", prpTest.getProperty("person"));
			assertEquals("täter", prpTest.getProperty("name"));

			assertNull(prpTest.getProperty("missing"));
			assertNull(prpTest.getProperty(""));

		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Tests app.
	 */
	@Test
	public void testApp() {

		Properties prpTest = null;

		try {
			prpTest = AppProperties.getProperties((String) null, APP, false);

			assertNotNull(prpTest);
			assertEquals(1, prpTest.size());
			assertArrayEquals(new String[]{"color"}, Collections.list(prpTest.propertyNames()).toArray());

			assertEquals("red", prpTest.getProperty("color"));
			assertNull(prpTest.getProperty("person"));
			assertNull(prpTest.getProperty("name"));

			assertNull(prpTest.getProperty("missing"));
			assertNull(prpTest.getProperty(""));

		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Tests both.
	 */
	@Test
	public void testBoth() {

		Properties prpTest = null;

		try {
			prpTest = AppProperties.getProperties(DEFAULT, APP, false);

			assertNotNull(prpTest);
			assertEquals(1, prpTest.size());
			assertEquals(Set.of("color", "person", "name"), new HashSet<>(Collections.list((Enumeration<String>) prpTest.propertyNames())));

			assertEquals("red", prpTest.getProperty("color"));
			assertEquals("me", prpTest.getProperty("person"));
			assertEquals("täter", prpTest.getProperty("name"));

			assertNull(prpTest.getProperty("missing"));
			assertNull(prpTest.getProperty(""));

		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Tests change.
	 */
	@Test
	public void testChange() {

		Properties prpTest = null;

		try {
			prpTest = AppProperties.getProperties(DEFAULT, APP, false);

			assertNotNull(prpTest);
			assertEquals(1, prpTest.size());
			assertEquals(Set.of("color", "person", "name"), new HashSet<>(Collections.list((Enumeration<String>) prpTest.propertyNames())));

			assertEquals("red", prpTest.getProperty("color"));
			assertEquals("me", prpTest.getProperty("person"));
			assertEquals("täter", prpTest.getProperty("name"));

			assertNull(prpTest.getProperty("missing"));
			assertNull(prpTest.getProperty(""));

			prpTest.setProperty("color", "grün");
			prpTest.setProperty("person", "Bär");
			prpTest.setProperty("foo", "foo");

			AppProperties.saveProperties(prpTest, APP, "Comment ~~");

			prpTest = AppProperties.getProperties(DEFAULT, APP, false);

			assertNotNull(prpTest);
			assertEquals(3, prpTest.size());
			assertEquals(Set.of("color", "person", "name", "foo"), new HashSet<>(Collections.list((Enumeration<String>) prpTest.propertyNames())));

			assertEquals("grün", prpTest.getProperty("color"));
			assertEquals("Bär", prpTest.getProperty("person"));
			assertEquals("täter", prpTest.getProperty("name"));
			assertEquals("foo", prpTest.getProperty("foo"));

			assertNull(prpTest.getProperty("missing"));
			assertNull(prpTest.getProperty(""));

		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Tests default (Properties).
	 */
	@Test
	public void testDefaultProperties() {

		Properties prpDefault = new Properties();
		prpDefault.setProperty("color", "blue");
		prpDefault.setProperty("person", "me");
		prpDefault.setProperty("name", "täter");

		try {
			Properties prpTest = AppProperties.getProperties(prpDefault, null, false);

			assertNotNull(prpTest);
			assertEquals(0, prpTest.size());
			assertEquals(Set.of("color", "person", "name"), new HashSet<>(Collections.list((Enumeration<String>) prpTest.propertyNames())));

			assertEquals("blue", prpTest.getProperty("color"));
			assertEquals("me", prpTest.getProperty("person"));
			assertEquals("täter", prpTest.getProperty("name"));

			assertNull(prpTest.getProperty("missing"));
			assertNull(prpTest.getProperty(""));

		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

}

/* EOF */
