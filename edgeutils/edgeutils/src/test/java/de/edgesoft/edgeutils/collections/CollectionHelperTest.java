package de.edgesoft.edgeutils.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.junit.jupiter.api.Test;

/**
 * Unit test for {@link CollectionHelper}.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.12.0
 */
@SuppressWarnings("static-method")
public class CollectionHelperTest {

	/**
	 * Tests {@link CollectionHelper#toCSVString(java.util.Collection, String)}.
	 */
	@Test
	public void testtoCSVString() {

		assertEquals("", CollectionHelper.toCSVString(null, null));
		assertEquals("", CollectionHelper.toCSVString(Collections.emptyList(), null));
		assertEquals("", CollectionHelper.toCSVString(null, "."));
		assertEquals("", CollectionHelper.toCSVString(Collections.emptyList(), "."));

		assertEquals("a.b.c", CollectionHelper.toCSVString(Arrays.asList(new String[] {"a", "b", "c"}), "."));
		assertEquals("a#b#c", CollectionHelper.toCSVString(Arrays.asList(new String[] {"a", "b", "c"}), "#"));

		assertEquals("a .b .c", CollectionHelper.toCSVString(Arrays.asList(new String[] {"a", "b", "c"}), " ."));
		assertEquals("a . b . c", CollectionHelper.toCSVString(Arrays.asList(new String[] {"a", "b", "c"}), " . "));

	}

	/**
	 * Tests {@link CollectionHelper#fromCSVString(String, String)}.
	 */
	@Test
	public void testfromCSVString() {

		assertNull(CollectionHelper.fromCSVString(null, null));
		assertNull(CollectionHelper.fromCSVString("a.b.c", null));
		assertNull(CollectionHelper.fromCSVString(null, "."));

		assertIterableEquals(Arrays.asList(new String[] {""}), CollectionHelper.fromCSVString("", "."));
		assertIterableEquals(Arrays.asList(new String[] {""}), CollectionHelper.fromCSVString("   ", "."));

		assertIterableEquals(Arrays.asList(new String[] {"", ""}), CollectionHelper.fromCSVString(".", "."));

		assertIterableEquals(Arrays.asList(new String[] {"a"}), CollectionHelper.fromCSVString("a", "."));
		assertIterableEquals(Arrays.asList(new String[] {"a"}), CollectionHelper.fromCSVString(" a  ", "."));

		assertIterableEquals(Arrays.asList(new String[] {"a", ""}), CollectionHelper.fromCSVString("a.", "."));
		assertIterableEquals(Arrays.asList(new String[] {"a", ""}), CollectionHelper.fromCSVString(" a . ", "."));

		assertIterableEquals(Arrays.asList(new String[] {"a", "b", "c"}), CollectionHelper.fromCSVString("a.b.c", "."));

		assertIterableEquals(Arrays.asList(new String[] {"a", "", "", "", "b", ""}), CollectionHelper.fromCSVString(" a . ...b.", "."));

	}

	/**
	 * Tests {@link CollectionHelper#trimSplit(String, String)}.
	 */
	@Test
	public void testtrimSplit() {

		assertNull(CollectionHelper.trimSplit(null, null));
		assertNull(CollectionHelper.trimSplit("a.b.c", null));
		assertNull(CollectionHelper.trimSplit(null, "."));

		assertIterableEquals(Collections.emptyList(), CollectionHelper.trimSplit("", "."));
		assertIterableEquals(Collections.emptyList(), CollectionHelper.trimSplit("   ", "."));

		assertIterableEquals(Arrays.asList(new String[] {"", ""}), CollectionHelper.fromCSVString(".", "."));

		assertIterableEquals(Arrays.asList(new String[] {"a"}), CollectionHelper.trimSplit("a", "."));
		assertIterableEquals(Arrays.asList(new String[] {"a"}), CollectionHelper.trimSplit(" a  ", "."));

		assertIterableEquals(Arrays.asList(new String[] {"a"}), CollectionHelper.trimSplit("a.", "."));
		assertIterableEquals(Arrays.asList(new String[] {"a"}), CollectionHelper.trimSplit(" a . ", "."));

		assertIterableEquals(Arrays.asList(new String[] {"a", "b", "c"}), CollectionHelper.trimSplit("a.b.c", "."));

		assertIterableEquals(Arrays.asList(new String[] {"a", "", "", "", "b"}), CollectionHelper.trimSplit(" a . ...b.", "."));

	}

	/**
	 * Tests {@link CollectionHelper#countOccurrences(String, char)}.
	 */
	@Test
	public void testcountOccurrences() {

		assertEquals(-1, CollectionHelper.countOccurrences(null, '.'));

		assertEquals(0, CollectionHelper.countOccurrences("", '.'));
		assertEquals(0, CollectionHelper.countOccurrences("   ", '.'));
		assertEquals(0, CollectionHelper.countOccurrences("abc", '.'));

		assertEquals(1, CollectionHelper.countOccurrences(".", '.'));
		assertEquals(1, CollectionHelper.countOccurrences("  .  ", '.'));
		assertEquals(1, CollectionHelper.countOccurrences("abc . def \n", '.'));

		assertEquals(1, CollectionHelper.countOccurrences("abc . def \n", '\n'));

		assertEquals(5, CollectionHelper.countOccurrences(".....", '.'));

	}

	/**
	 * Tests {@link CollectionHelper#isEmptyString(java.util.Collection)}.
	 */
	@Test
	public void testisEmptyString() {

		assertFalse(CollectionHelper.isEmptyString(null));

		assertTrue(CollectionHelper.isEmptyString(Collections.emptyList()));
		assertTrue(CollectionHelper.isEmptyString(new ArrayList<>()));
		assertTrue(CollectionHelper.isEmptyString(Arrays.asList(new String[] {""})));
		assertTrue(CollectionHelper.isEmptyString(Arrays.asList(new String[] {"", ""})));
		assertTrue(CollectionHelper.isEmptyString(Arrays.asList(new String[] {"", "", "", "", "", "", ""})));

		assertFalse(CollectionHelper.isEmptyString(Arrays.asList(new String[] {"."})));
		assertFalse(CollectionHelper.isEmptyString(Arrays.asList(new String[] {"", "", "", "", "", "."})));
		assertFalse(CollectionHelper.isEmptyString(Arrays.asList(new String[] {".", "", "", "", "", ""})));
		assertFalse(CollectionHelper.isEmptyString(Arrays.asList(new String[] {"", "", "", ".", ""})));

	}

	/**
	 * Tests {@link CollectionHelper#getFilledList(Object, int)}.
	 */
	@Test
	public void testgetFilledList() {

		assertNull(CollectionHelper.getFilledList(null, 1));
		assertNull(CollectionHelper.getFilledList(null, -1));

		assertIterableEquals(Collections.emptyList(), CollectionHelper.getFilledList(".", -1));
		assertIterableEquals(Collections.emptyList(), CollectionHelper.getFilledList(".", 0));

		assertIterableEquals(Arrays.asList(new String[] {"."}), CollectionHelper.getFilledList(".", 1));
		assertIterableEquals(Arrays.asList(new String[] {".", ".", ".", ".", ".", ".", "."}), CollectionHelper.getFilledList(".", 7));

	}

}

/* EOF */
