package de.edgesoft.edgeutils.commandline;

/**
 * Test class for AbstractMainClass.
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 * 
 * <p>This file is part of edgeutils.</p>
 * 
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 * 
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 * 
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 * 
 * @author Ekkart Kleinod
 * @since 0.4.0
 */
public class TestMainClass extends AbstractMainClass {
	
	/**
	 * Dummy public field for field getting test.
	 */
	public String sDummy1 = null;
	
	/**
	 * Dummy public field for field getting test.
	 */
	public String sDummy2 = null;
	
	/**
	 * Command line entry point.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		new TestMainClass().executeOperation(args);
	}

	/**
	 * Programmatic entry point, initializing and executing main functionality.
	 * 
	 * - set description setDescription("...");
	 * - add options addOption("short", "long", "description", argument?, required?);
	 * - call init(args);
	 * - call operation execution with arguments
	 */
	@Override
	public void executeOperation(String[] args) {
		
		setDescription("JUnit test class.");
		
		addOption("n", "non-argument", "optional parameter without argument", false, false);
		addOption("a", "argument", "optional parameter with argument", true, false);
		addOption("r", "required", "required parameter without argument", false, true);
		addOption("t", "true", "required parameter with argument", true, true);
		
		init(args);
		
		executeOperation(hasOption("n"), getOptionValue("a"), hasOption("r"), getOptionValue("t"));
	}

	/**
	 * Programmatic entry point for executing main functionality.
	 * 
	 * @param theN n argument
	 * @param theA a argument
	 * @param theR r argument
	 * @param theT t argument
	 */
	public void executeOperation(boolean theN, String theA, boolean theR, String theT) {
		// not important for tests
	}
		
}

/* EOF */
