package de.edgesoft.edgeutils.javafx;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javafx.embed.swing.JFXPanel;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

/**
 * Unit test for {@link TreeViewUtils}.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.12.0
 */
@SuppressWarnings("static-method")
public class TreeViewUtilsTest {

	/** Tree view. */
	private static TreeView<String> treeTest = null;

	/**
	 * Prepare JavaFX toolkit and environment.
	 */
	@BeforeAll
	public static void prepareJavaFX() {
		new JFXPanel();
	}

	/**
	 * Creates test data before each test.
	 */
	@BeforeEach
	public void createTestData() {

		treeTest = new TreeView<>();

		TreeItem<String> itmRoot = new CheckBoxTreeItem<>("root");
		treeTest.setRoot(itmRoot);

		itmRoot.getChildren().add(new CheckBoxTreeItem<>("checked1", null, true));
		itmRoot.getChildren().add(new CheckBoxTreeItem<>("unchecked1", null, false));
		itmRoot.getChildren().add(new CheckBoxTreeItem<>("checked2", null, true));
		itmRoot.getChildren().add(new CheckBoxTreeItem<>("unchecked2", null, false));
		itmRoot.getChildren().add(new CheckBoxTreeItem<>("unchecked3", null, false));

		itmRoot.getChildren().get(1).getChildren().add(new CheckBoxTreeItem<>("checked3", null, true));
		itmRoot.getChildren().get(1).getChildren().add(new CheckBoxTreeItem<>("checked4", null, true));
		itmRoot.getChildren().get(1).getChildren().add(new CheckBoxTreeItem<>("unchecked4", null, false));

		itmRoot.getChildren().get(2).getChildren().add(new CheckBoxTreeItem<>("unchecked5", null, false));
		itmRoot.getChildren().get(2).getChildren().add(new CheckBoxTreeItem<>("checked5", null, true));
		itmRoot.getChildren().get(2).getChildren().add(new CheckBoxTreeItem<>("unchecked6", null, false));

		itmRoot.getChildren().get(2).getChildren().get(0).getChildren().add(new CheckBoxTreeItem<>("checked6", null, true));
		itmRoot.getChildren().get(2).getChildren().get(0).getChildren().add(new CheckBoxTreeItem<>("checked7", null, true));
		itmRoot.getChildren().get(2).getChildren().get(0).getChildren().add(new CheckBoxTreeItem<>("unchecked7", null, false));

	}

	/**
	 * Tests {@link TreeViewUtils#getCheckedCheckBoxTreeItems(TreeView)}.
	 */
	@Test
	public void testgetCheckedCheckBoxTreeItems() {

		assertEquals(7, TreeViewUtils.getCheckedCheckBoxTreeItems(treeTest).size());
		assertEquals("[TreeItem [ value: checked1 ], TreeItem [ value: checked3 ], TreeItem [ value: checked4 ], TreeItem [ value: checked2 ], TreeItem [ value: checked6 ], TreeItem [ value: checked7 ], TreeItem [ value: checked5 ]]", TreeViewUtils.getCheckedCheckBoxTreeItems(treeTest).toString());

	}

}

/* EOF */
