package de.edgesoft.edgeutils.files;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Unit test for {@link FileUtils}.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2010-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of edgeutils.</p>
 *
 * <p>edgeutils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>edgeutils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with edgeutils. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.9.7
 */
@SuppressWarnings("static-method")
public class FileUtilsTest {

	/**
	 * Tests {@link FileUtils#cleanFilename(String)}.
	 *
	 */
	@Test
	public void testCleanFilename() {

		assertEquals("abc", FileUtils.cleanFilename("abc"));
		assertEquals("aeoeue", FileUtils.cleanFilename("äöü"));
		assertEquals("ss_lou", FileUtils.cleanFilename("ß lou"));
		assertEquals("_l_.._", FileUtils.cleanFilename("_l;.._"));
		assertEquals("___._", FileUtils.cleanFilename("?:;.,"));
		assertEquals("__", FileUtils.cleanFilename("/\\"));

	}

}

/* EOF */
