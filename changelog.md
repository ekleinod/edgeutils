# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.17.0] - 2022-12-04

### Changed

- replace dash with minus in filenames in `FileUtils`


## [0.16.0] - 2022-06-03

### Changed

- updated `commons-cli`: 1.4 -> 1.5.0
- updated `freemarker`: 2.3.29 -> 2.3.31
- updated `testfx`: 4.0.15-alpha -> 4.0.16-alpha

### Security

- updated `log4j`: 2.11.2 -> 2.17.2 (vulnerability)



## [0.15.0] - 2022-06-02

### Added

- workflow description in documentation
- ant commons: checks "isUnix" and "isWindows"
- using ant commons in NSIS task
- `TextField` now known in `ControlUtils`
- new ResourceType `PATTERN`
- UUID generation for `IDTypeExt` (issue #22)
- helper functions for processing freemarker templates (issue #23)
- new function `loadFXMLObject` for loading all FXML resources, not just parents (issue #24)
- new function in `Prefs` `put` that removes key if default value was used or value is blank/null (issue #25)
- new function in `Prefs` `remove` that removes a key (issue #26)

### Changed

- improved documentation

### Fixed

- ant dpkg: debian installer creation
- Resources class now throws an error for missing resources
- typos in docs


## [0.14.0] - 2021-04-05

### Added

- prefix for EdgeDatePicker
- online documentation
- release to maven repository

### Changed

- ant-setversion.xml -> ant-setcopyright.xml


## [0.13.0] - 2020-06-06

### Added

- new classes
	- AppenderUtils
	- CheckBoxUtils
	- EdgeDatePicker
	- SceneUtils
	- TableColumnUtils
	- TabUtils
	- ThrowableUtils
	- TimeTextField
	- TooltipUtils
- I18N classes and interfaces
- Resources method for getting a file list

### Changed

- switched to Java 12
- updates of included libraries
- separated CopyTextPane from HyperlinkController
- FileUtils sanitize filename: added stars and brackets
- latex
  - ant xelatex and luatex
  - ant documentation targets
  - documentation definitions

### Fixed

- eclipse project name "edgeutils-modules"
- bug in file writing, using encoding now
- LocalTimeAdapter now with ISO_LOCAL_TIME formatting



## [0.12.0] - 2019-07-20

### Added

- information for maven repository publishing
- module structure for Java 11+
- basic i8n support
- resource loading class
- hyperlink controller for JavaFX
- support for split menu buttons

### Changed

- developer documentation
- ButtonUtils working on ButtonBase
- settings for new eclipse version
- MMDMarkup for versions 5 and 6
- extended DateTimeUtils.parseDateTime with ISO offset date/time



## [0.11.0] - 2018-08-26

### Added

- ComboBoxUtils
- FileUtils
	- methods to check if file exists based on paths
- XYSeriesUtils
- LabelUtils
- FontUtils
- Unit-Tests
	- tests via surefire
	- more tests
- TestFX helper classes
	- KeyCodeCombinationUtils
	- RobotHelper
	- CheckBoxMatcher
	- ComboBoxMatcher
	- DatePickerMatcher
	- TextInputControlMatcher
- packageinfo for some packages
- ant task for ducktype/mallard
- developer documentation

### Changed

- license change from LGPL to GPL
- git branching model canged to mainline model
- changelog style
- NSIS
	- autostart optional
	- ant task extended by docversion change
- FileUtils
	- keep dots in filenames
	- cleaning filenames without umlauts as option
- updated maven dependency versions
- ClassUtils
	- returning declared fields only if not private
- ButtonUtils
	- button method for binding disable to list view selection
	- bind disable for buttons and comboboxes
	- ButtonUtils null check for accelerator
- Unit-Tests
	- switch to JUnit 5
- white space in java files is removed automatically via eclipse
- separated ant tasks for NSIS, setversion, and debinstall

### Removed

- NSIS
	- obsolete reserve file
- removed version from filenames in installers (NSIS, deb)


## [0.10.1] - 2017-07-16

### Fixed

- bugfix NSIS generation


## [0.10.0] - 2017-07-16

### Added

- more utilities for JavaFX classes
- new AbstractModelClass in order to avoid problems of extending ModelClass, see comment in commons.xsd
- NSIS ant build and simple jar script

### Changed

- updated XChart helper classes
- build scripts


## [0.9.8] - 2017-04-18

### Added

- XChart helper classes
- new model class ModelClassExt for standard getDisplayText method

### Changed

- improved DateTimeUtils


## [0.9.7] - 2017-02-05

### Added

- DoublePropertyAdapter
- FileUtils

### Changed

- improved DateTimeUtils
- started cleaner ant commons


## [0.9.6] - 2016-10-13

### Added

- adapters, elements, and bindings for most used JavaFX properties

### Changed

- improved test of commons


## [0.9.5] - 2016-09-21

### Changed

- split PropertyUtils into two XMLAdapters


## [0.9.4] - 2016-09-20

### Added

- PropertyUtils for marshaling/unmarshaling JavaFX properties


## [0.9.3] - 2016-09-16

### Added

- ColorUtils for formatting colors


## [0.9.2] - 2016-09-15

### Changed

- extending DateTimeUtils with formatting/parsing dates


## [0.9.1] - 2016-08-08

### Added

- method for parsing date and/or time strings


## [0.9.0] - 2016-08-07

### Added

- class for DateTime convenience methods


## [0.8.0] - 2016-08-02

### Changed

- AppProperties with default properties as Properties object


## [0.7.3] - 2016-08-02

### Fixed

- hotfix: forgot to create model classes


## [0.7.2] - 2016-08-02

### Changed

- id of IDType may be optional


## [0.7.1] - 2016-07-24

### Added

- introduced base class for all model classes, for easier identification of model classes in source code


## [0.7.0] - 2016-07-20

### Removed

- removed confusing "Test" suffixes


## [0.6.1] - 2016-07-19

### Fixed

- hotfix: tests run under windows now (German windows)


## [0.6.0] - 2016-07-11

### Added

- XML bindings of date, time, and datetime to LocalDate, LocalTime, and LocalDateTime

### Changed

- version class with toString
- call of xjc always with three bindings


## [0.5.1] - 2016-06-30

### Fixed

- hotfix: filenames are paths now instead of strings


## [0.5.0] - 2016-06-30

### Added

- property file class
- started with unit tests

### Changed

- file access based on Java 8


## [0.4.0] - 2016-05-16

### Changed

- using maven
- no jars anymore, deployment via maven


## [0.3.1] - 2015-10-23

### Fixed

- Hotfix: timestamp not abstract


## [0.3.0] - 2014-12-10

### Added

- containing package for markup
- new `commons.xsd` containing common xml schema structures/types
- new `commons.xjb` containing common jaxb bindings
- readme for jaxb file usage


## [0.2.0] - 2014-12-02

### Added

- binary export to jar (for use of edgeutils in submodules)
- reveal markup

### Changed

- jaxb unmarshall with includes

### Fixed

- bugfixes: license, error printing


## [0.1.0] - 2014-10-31

### Added

- Classes for easing the use of
	- command line parameters
	- file access
	- LaTeX output
	- multimarkdown output
